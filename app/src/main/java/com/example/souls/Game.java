package com.example.souls;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.souls.ClassSelection.picked_class;
import static com.example.souls.GameView.mapnumber;
import static com.example.souls.GameView.player_type_archer;
import static com.example.souls.GameView.player_type_assassin;
import static com.example.souls.GameView.player_type_mage;
import static com.example.souls.MainActivity.GAME_STATS;
import static com.example.souls.MainActivity.S_class;
import static com.example.souls.MainActivity.S_mapnumber;

public class Game extends AppCompatActivity implements View.OnTouchListener {

    public static boolean isLeftPressed = false;
    public static boolean isRightPressed = false;
    public static boolean isTopPressed = false;
    public static boolean isBotPressed = false;
    public static boolean isAttackPressed = false;
    public static boolean isSpellPressed = false;
    public static ImageButton MoveLeftButton;
    public static ImageButton MoveRightButton;
    public static ImageButton MoveTopButton;
    public static ImageButton MoveBotButton;
    public static ImageButton AttackButton;
    public static ImageButton SpellButton;
    public static TextView PlayerHp;
    public static TextView BossHpBar;
    public static boolean StopThread=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.game_layout);
        GameView gameview = new GameView(this);
        LinearLayout gameLayout = (LinearLayout) findViewById(R.id.gameLayout);
        gameLayout.addView(gameview);

        PlayerHp =  findViewById(R.id.playerhpbar);
        BossHpBar =  findViewById(R.id.bosshpbar);
        MoveLeftButton =  findViewById(R.id.moveLeftButton);
        MoveRightButton = findViewById(R.id.moveRightButton);
        MoveTopButton =  findViewById(R.id.moveTopButton);
        MoveBotButton =  findViewById(R.id.moveBotButton);
        AttackButton=  findViewById(R.id.attackButton);
        SpellButton = findViewById(R.id.spellButton);

        MoveLeftButton.setOnTouchListener(this);
        MoveRightButton.setOnTouchListener(this);
        MoveTopButton.setOnTouchListener(this);
        MoveBotButton.setOnTouchListener(this);
        AttackButton.setOnTouchListener(this);
        SpellButton.setOnTouchListener(this);

    }


    @Override
    public boolean onTouch(View btn, MotionEvent event) {
        switch(btn.getId()) { // определяем какая кнопка
            case R.id.moveLeftButton:
                switch (event.getAction()) { // определяем нажата или отпущена
                    case MotionEvent.ACTION_DOWN:
                        isLeftPressed = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        isLeftPressed = false;
                        break;
                }
                break;
            case R.id.moveRightButton:
                switch (event.getAction()) { // определяем нажата или отпущена
                    case MotionEvent.ACTION_DOWN:
                        isRightPressed = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        isRightPressed = false;
                        break;
                }
                break;

            case R.id.moveTopButton:
                switch (event.getAction()) { // определяем нажата или отпущена
                    case MotionEvent.ACTION_DOWN:
                        isTopPressed = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        isTopPressed = false;
                        break;
                }
                break;

            case R.id.moveBotButton:
                switch (event.getAction()) { // определяем нажата или отпущена
                    case MotionEvent.ACTION_DOWN:
                        isBotPressed = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        isBotPressed = false;
                        break;
                }
                break;
            case R.id.attackButton:
                switch (event.getAction()) { // определяем нажата или отпущена
                    case MotionEvent.ACTION_DOWN:
                        isAttackPressed = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        isAttackPressed = false;
                        break;
                }
                break;
            case R.id.spellButton:
                switch (event.getAction()) { // определяем нажата или отпущена
                    case MotionEvent.ACTION_DOWN:
                        isSpellPressed = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        isSpellPressed = false;
                        break;
                }
                break;
        }

        return true;
    }

    public static void changeIcons(int player_type){

        switch (player_type){
            case player_type_archer:
                AttackButton.setBackgroundResource(R.drawable.shoticon);
                SpellButton.setBackgroundResource(R.drawable.multishoticon);
                break;
            case player_type_mage:
                AttackButton.setBackgroundResource(R.drawable.fireballicon);
                SpellButton.setBackgroundResource(R.drawable.cataclysmicon);
                break;
            case player_type_assassin:
                AttackButton.setBackgroundResource(R.drawable.bladefuryicon);
                SpellButton.setBackgroundResource(R.drawable.blinkicon);

                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(mapnumber==8 || mapnumber==9){
            Toast leave = Toast.makeText(this,R.string.demonking_leave, Toast.LENGTH_LONG);
            leave.show();
        }
        GAME_STATS = getApplicationContext().getSharedPreferences("settings_key", MODE_PRIVATE);
        SharedPreferences.Editor ed = GAME_STATS.edit();

        ed.putInt(S_mapnumber, mapnumber );
        ed.apply ();
        ed.putInt(S_class, picked_class);
        ed.apply();
        StopThread=true;

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public static void SetBossHp(int hp){
        BossHpBar.setText(hp + "HP");
    }
    public static void SetPlayersHp(int hp){
        PlayerHp.setText(hp + "HP");
    }

    @Override
    protected void onUserLeaveHint() {//ToDO возобновление игры
        if(mapnumber==8 || mapnumber==9){
            Toast leave = Toast.makeText(this,R.string.demonking_leave, Toast.LENGTH_LONG);
            leave.show();
        }
        GAME_STATS = getApplicationContext().getSharedPreferences("settings_key", MODE_PRIVATE);
        SharedPreferences.Editor ed = GAME_STATS.edit();

        ed.putInt(S_mapnumber, mapnumber );
        ed.apply ();
        ed.putInt(S_class, picked_class);
        ed.apply();
        StopThread=true;

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        super.onUserLeaveHint();
    }


}
