package com.example.souls;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import static com.example.souls.Game.StopThread;
import static com.example.souls.GameView.mapnumber;

public class ClassSelection extends AppCompatActivity implements View.OnClickListener {

    public static int picked_class;
    Button pick_archer;
    Button pick_mage;
    Button pick_assassin;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.class_selection);
        pick_archer = (Button) findViewById(R.id.select_archer);
        pick_mage = (Button) findViewById(R.id.select_mage);
        //pick_assassin = (Button) findViewById(R.id.select_assassin);
        pick_archer.setOnClickListener(this);
        pick_mage.setOnClickListener(this);
        //pick_assassin.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.select_archer:
                picked_class=1;
                Intent intent = new Intent(this, Intro.class);
                startActivity(intent);
                break;
            case R.id.select_mage:
                picked_class=2;
                Intent intent1 = new Intent(this, Intro.class);
                startActivity(intent1);
                break;
            /*case R.id.select_assassin:
                picked_class=3;
                Intent intent2 = new Intent(this, Intro.class);
                startActivity(intent2);
                break;*/
        }

    }
}
