package com.example.souls;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static com.example.souls.ClassSelection.picked_class;
import static com.example.souls.Game.StopThread;
import static com.example.souls.GameView.mapnumber;

public class Intro extends AppCompatActivity implements View.OnClickListener {

    Button StartGame;
    TextView IntroText;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro);
        StartGame = findViewById(R.id.intro_start_game);
        IntroText = findViewById(R.id.intro_text);
        StartGame.setOnClickListener(this);
        switch (picked_class){
            case 1:
                IntroText.setText(R.string.archer_intro);
                break;
            case 2:
                IntroText.setText(R.string.mage_intro);
                break;
        }
    }
    @Override
    public void onClick(View v) {
        mapnumber=0;
        StopThread=false;
        Intent intent1 = new Intent(this, Game.class);
        startActivity(intent1);
    }
}
