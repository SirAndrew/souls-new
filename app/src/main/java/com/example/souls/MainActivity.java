package com.example.souls;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import static com.example.souls.ClassSelection.picked_class;
import static com.example.souls.Game.StopThread;
import static com.example.souls.GameView.mapnumber;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button StartGame;
    Button ContinueGame;
    public static String TAG = "KEK";
    public static SharedPreferences GAME_STATS;
    final static String S_mapnumber = "Smapnumber";
    final static String S_class = "Sclass";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StartGame =(Button) findViewById(R.id.start_play_button);
        ContinueGame= findViewById(R.id.continue_play_button);
        StartGame.setOnClickListener(this);
        ContinueGame.setOnClickListener(this);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.start_play_button:
                Intent intent = new Intent(this, ClassSelection.class);
                startActivity(intent);
                break;
            case R.id.continue_play_button:
                GAME_STATS = getApplicationContext().getSharedPreferences("settings_key", MODE_PRIVATE);
                //mapnumber = GAME_STATS.getInt(S_mapnumber,0); //ToDo временно для отладки
                mapnumber=2;
                picked_class = GAME_STATS.getInt(S_class,0);
                StopThread=false;
                Intent intent1 = new Intent(this, Game.class);
                startActivity(intent1);
                break;

        }
    }
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
