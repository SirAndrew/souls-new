package com.example.souls.MapPool;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.souls.GameView;
import com.example.souls.R;

import static com.example.souls.GameView.currentMap;

public class Map9 {
    public   int bitmapId = R.drawable.map9; // id картинки
    protected Bitmap bitmapMap; // картинка
    public  int [] [] map =  {
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,0,1,0,0,0,1,1,1,0,0,0,1,0,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,0,0,0,1,0,0,0,0,0,1,0,0,0,1},
            {1,0,0,0,1,0,0,0,0,0,1,0,0,0,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,0,0,0,1,0,0,0,0,0,1,0,0,0,1},
            {1,0,0,0,1,0,0,0,0,0,1,0,0,0,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,0,0,0,1,0,0,0,0,0,1,0,0,0,1},
            {1,0,0,0,1,0,0,0,0,0,1,0,0,0,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,0,1,0,0,0,1,1,1,0,0,0,1,0,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
    };
    public static int [] [] archerStartPositions = {
            {1},
            {1},
            {1,}
    };
    public static int [] [] mageStartPositions = {
            {13},
            {1},
            {1}
    };
    public static int [] [] freezemageStartPositions = {
            {},
            {},
            {}
    };

    public static int [] [] bossArcherStartPositions = {
            {},
            {},
            {}
    };
    public static int [] [] bossMageStartPositions = {
            {},
            {},
            {}
    };
    public static int [] [] bossFreezeMageStartPositions = {
            {},
            {},
            {}
    };
    public static int [] [] demonKingStartPositions = {
            {},
            {},
            {}
    };
    public static int [][] trueDemonKingStartPositions = {
            {7},
            {4},
            {1}
    };
    //0 вверх, 1 влево, 2 вниз, 3 вправо
    public static int [] [] fireballStartPositions = {  {}, //координата X
            {}, //Координата Y
            {}};//Направление
    public static int [] [] arrowStartPositions = { };
    public static int [] infoenemy = {0,0,1,1,0,0,0,0,0,0,0,0}; //количество фаерболов, стрел, лучников, магов, ледяных магов, камикадзе, босс лучник, мишень, босс маг, лич
    public static int [] nextLevel = {0,0};
    public static int [] playerStartPosition= {7,11,0};
    public static int [] autosave = {0,0};

    void init(Context context) { // сжимаем картинку до нужных размеров
        Bitmap cBitmap = BitmapFactory.decodeResource(context.getResources(), bitmapId);
        bitmapMap = Bitmap.createScaledBitmap(
                cBitmap, (int)(GameView.unitX*GameView.cellX), (int)(GameView.unitY*GameView.cellY), false);
    }


    public Map9 (Context context){

    }

    public void equate(){
        currentMap.bitmapId=this.bitmapId;
        currentMap.map=this.map;
        currentMap.archerStartPositions=this.archerStartPositions;
        currentMap.mageStartPositions=this.mageStartPositions;
        currentMap.freezemageStartPositions=this.freezemageStartPositions;
        currentMap.bossArcherStartPositions=this.bossArcherStartPositions;
        currentMap.bossMageStartPositions=this.bossMageStartPositions;
        currentMap.bossFreezeMageStartPositions=this.bossFreezeMageStartPositions;
        currentMap.demonKingStartPositions=this.demonKingStartPositions;
        currentMap.trueDemonKingStartPositions=this.trueDemonKingStartPositions;
        currentMap.fireballStartPositions=this.fireballStartPositions;
        currentMap.arrowStartPositions=this.arrowStartPositions;
        currentMap.infoenemy=this.infoenemy;
        currentMap.playerStartPosition=this.playerStartPosition;
        currentMap.nextLevel=this.nextLevel;
        currentMap.autosave=this.autosave;

    }
}
