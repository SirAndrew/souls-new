package com.example.souls.MapPool;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.souls.GameView;
import com.example.souls.R;

import static com.example.souls.GameView.currentMap;

public class Map3 {

    public   int bitmapId = R.drawable.map3; // id картинки
    protected Bitmap bitmapMap; // картинка
    public int [] [] map =  {
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,0,0,0,0,0,1,0,0,0,0,0,0,0,1},
            {1,0,0,0,0,0,1,0,0,0,0,0,0,0,1},
            {1,0,0,0,0,0,1,0,0,0,0,0,0,0,1},
            {1,0,0,0,0,0,1,0,0,1,1,1,1,1,1},
            {1,0,0,0,0,0,1,0,0,1,0,0,1,0,1},
            {1,0,0,0,0,0,1,0,0,1,0,0,1,0,1},
            {1,0,0,0,0,0,1,1,1,1,0,0,1,0,1},
            {1,0,0,0,0,0,0,0,0,1,0,0,1,0,1},
            {1,0,1,1,1,1,1,0,0,1,0,0,1,0,1},
            {1,0,0,0,0,0,1,0,0,1,0,0,1,0,1},
            {1,0,0,0,0,0,1,1,1,1,0,0,1,0,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,1,0,1},
            {1,0,1,1,1,1,1,1,1,1,1,1,1,0,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
    };
    public static int [] [] archerStartPositions = {
            {1,10,5},
            {1,5, 11},
            {1,1,1}
    };
    public static int [] [] mageStartPositions = {
            {13,7,8},
            {1, 4,10},
            {1, 1,1}
    };
    public static int [] [] freezemageStartPositions = {
            {},
            {},
            {}
    };

    public static int [] [] bossArcherStartPositions = {
            {},
            {},
            {}
    };
    public static int [] [] bossMageStartPositions = {
            {},
            {},
            {}
    };
    public static int [] [] bossFreezeMageStartPositions = {
            {},
            {},
            {}
    };
    public static int [] [] demonKingStartPositions = {
            {},
            {},
            {}
    };
    public static int [][] trueDemonKingStartPositions = {
            {},
            {},
            {}
    };
    public static int [] [] fireballStartPositions = { };
    public static int [] [] arrowStartPositions = { };
    public static int [] infoenemy = {0,0,3,0,0,0,0,1,0,0,0,0}; //количество фаерболов, стрел, лучников, магов, ледяных магов, камикадзе, босс лучник, мишень, босс маг, лич
    public static int [] nextLevel = {13,1};
    public static int [] playerStartPosition= {8,10,0};
    public static int [] autosave = {0,0};

    void init(Context context) { // сжимаем картинку до нужных размеров
        Bitmap cBitmap = BitmapFactory.decodeResource(context.getResources(), bitmapId);
        bitmapMap = Bitmap.createScaledBitmap(
                cBitmap, (int)(GameView.unitX*GameView.cellX), (int)(GameView.unitY*GameView.cellY), false);
    }


    public Map3 (Context context){

    }

    public void equate(){
        currentMap.bitmapId=this.bitmapId;
        currentMap.map=this.map;
        currentMap.archerStartPositions=this.archerStartPositions;
        currentMap.mageStartPositions=this.mageStartPositions;
        currentMap.freezemageStartPositions=this.freezemageStartPositions;
        currentMap.bossArcherStartPositions=this.bossArcherStartPositions;
        currentMap.bossMageStartPositions=this.bossMageStartPositions;
        currentMap.bossFreezeMageStartPositions=this.bossFreezeMageStartPositions;
        currentMap.demonKingStartPositions=this.demonKingStartPositions;
        currentMap.trueDemonKingStartPositions=this.trueDemonKingStartPositions;
        currentMap.fireballStartPositions=this.fireballStartPositions;
        currentMap.arrowStartPositions=this.arrowStartPositions;
        currentMap.infoenemy=this.infoenemy;
        currentMap.playerStartPosition=this.playerStartPosition;
        currentMap.nextLevel=this.nextLevel;
        currentMap.autosave=this.autosave;

    }

}
