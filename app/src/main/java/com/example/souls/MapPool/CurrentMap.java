package com.example.souls.MapPool;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.souls.GameView;

public class CurrentMap {
    public static int bitmapId;
    protected Bitmap bitmapMap;
    public static int [] [] map =  {};
    public static int [] [] archerStartPositions = {  };
    public static int [] [] mageStartPositions = {  };
    public static int [] [] fireballStartPositions = {};//Направление
    public static int [] [] arrowStartPositions = { };
    public static int [] [] freezemageStartPositions = {};
    public static int [] [] bossArcherStartPositions = {};
    public static int [] [] bossMageStartPositions = {};
    public static int [] [] bossFreezeMageStartPositions = {};
    public static int [] [] demonKingStartPositions = {};
    public static int [] [] trueDemonKingStartPositions = {};
    public static int [] infoenemy = {}; //количество фаерболов, стрел, лучников, магов
    public static int [] playerStartPosition={};
    public static int [] nextLevel ={};
    public static int [] autosave = {};

    public void init(Context context) { // сжимаем картинку до нужных размеров
        Bitmap cBitmap = BitmapFactory.decodeResource(context.getResources(), bitmapId);
        bitmapMap = Bitmap.createScaledBitmap(
                cBitmap, (int)(GameView.unitX*GameView.cellX), (int)(GameView.unitY*GameView.cellY), false);
    }


    public CurrentMap (Context context){
    }

    public void drow(Paint paint, Canvas canvas){ // рисуем картинку
        canvas.drawBitmap(bitmapMap, 0, 0, paint);
    }
}
