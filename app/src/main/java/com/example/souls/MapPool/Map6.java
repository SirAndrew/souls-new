package com.example.souls.MapPool;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.souls.GameView;
import com.example.souls.R;

import static com.example.souls.GameView.currentMap;

public class Map6 {
    public   int bitmapId = R.drawable.map6; // id картинки
    protected Bitmap bitmapMap; // картинка
    public  int [] [] map =  {
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,0,0,0,0,1,0,0,0,1,1,1,1,0,1},
            {1,0,0,0,1,1,0,0,0,1,0,0,0,0,1},
            {1,0,0,0,0,0,0,0,0,1,0,1,1,1,1},
            {1,0,0,0,0,0,0,1,1,1,0,0,0,1,1},
            {1,0,0,0,0,0,0,1,0,0,0,0,0,0,1},
            {1,0,1,0,0,0,0,1,1,1,0,1,0,1,1},
            {1,0,0,1,0,0,0,0,0,1,1,0,0,1,1},
            {1,0,0,0,0,0,0,0,0,1,0,1,0,1,1},
            {1,0,0,0,0,0,0,0,0,1,0,1,0,0,1},
            {1,0,0,0,0,0,0,1,1,1,0,1,0,0,1},
            {1,0,0,0,0,0,0,1,0,1,0,1,0,1,1},
            {1,0,0,1,1,0,0,1,0,0,0,0,0,0,1},
            {1,0,0,0,0,0,0,1,1,1,0,1,0,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
    };
    public static int [] [] archerStartPositions = {
            {},
            {},
            {}
    };
    public static int [] [] mageStartPositions = {
            {2,8},
            {8,9},
            {1,1}
    };
    public static int [] [] freezemageStartPositions = {
            {2,4},
            {14,12},
            {1,1}
    };

    public static int [] [] bossArcherStartPositions = {
            {},
            {},
            {}
    };
    public static int [] [] bossMageStartPositions = {
            {},
            {},
            {}
    };
    public static int [] [] bossFreezeMageStartPositions = {
            {},
            {},
            {}
    };
    public static int [] [] demonKingStartPositions = {
            {},
            {},
            {}
    };
    public static int [][] trueDemonKingStartPositions = {
            {},
            {},
            {}
    };
    //0 вверх, 1 влево, 2 вниз, 3 вправо
    public static int [] [] fireballStartPositions = {  {1, 6, 8 ,8,10,10,12,13}, //координата X
                                                        {14,14,8,10,7, 14,14,6}, //Координата Y
                                                        {0, 0 ,1 ,1,0, 0 ,0,1}};//Направление
    public static int [] [] arrowStartPositions = { };
    public static int [] infoenemy = {8,0,0,0,0,0,0,0,0,0,0,0}; //количество фаерболов, стрел, лучников, магов, ледяных магов, камикадзе, босс лучник, мишень, босс маг, лич
    public static int [] nextLevel = {4,14};
    public static int [] playerStartPosition= {8,12,2};
    public static int [] autosave = {8,1};

    void init(Context context) { // сжимаем картинку до нужных размеров
        Bitmap cBitmap = BitmapFactory.decodeResource(context.getResources(), bitmapId);
        bitmapMap = Bitmap.createScaledBitmap(
                cBitmap, (int)(GameView.unitX*GameView.cellX), (int)(GameView.unitY*GameView.cellY), false);
    }


    public Map6 (Context context){

    }

    public void equate(){
        currentMap.bitmapId=this.bitmapId;
        currentMap.map=this.map;
        currentMap.archerStartPositions=this.archerStartPositions;
        currentMap.mageStartPositions=this.mageStartPositions;
        currentMap.freezemageStartPositions=this.freezemageStartPositions;
        currentMap.bossArcherStartPositions=this.bossArcherStartPositions;
        currentMap.bossMageStartPositions=this.bossMageStartPositions;
        currentMap.bossFreezeMageStartPositions=this.bossFreezeMageStartPositions;
        currentMap.demonKingStartPositions=this.demonKingStartPositions;
        currentMap.trueDemonKingStartPositions=this.trueDemonKingStartPositions;
        currentMap.fireballStartPositions=this.fireballStartPositions;
        currentMap.arrowStartPositions=this.arrowStartPositions;
        currentMap.infoenemy=this.infoenemy;
        currentMap.playerStartPosition=this.playerStartPosition;
        currentMap.nextLevel=this.nextLevel;
        currentMap.autosave=this.autosave;

    }
}
