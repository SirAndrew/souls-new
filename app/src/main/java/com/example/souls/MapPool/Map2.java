package com.example.souls.MapPool;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.souls.GameView;
import com.example.souls.R;

import static com.example.souls.GameView.currentMap;

public class Map2 {

    public static  int bitmapId = R.drawable.map2rework; // id картинки
    public static Bitmap bitmapMap; // картинка

    public static int [] [] map =  {
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,0,1,1,1,0,1,0,0,1,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,1,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,1,1},
            {1,1,0,0,0,1,1,1,1,1,0,0,0,1,1},
            {1,1,0,0,1,0,0,0,0,1,1,0,0,1,1},
            {1,0,0,0,1,0,0,0,0,1,1,0,0,1,1},
            {1,1,0,0,1,0,0,0,0,1,1,0,0,0,1},
            {1,1,0,0,0,0,0,0,0,0,1,0,0,1,1},
            {1,1,0,0,0,0,0,0,0,1,1,0,0,1,1},
            {1,1,1,1,0,0,0,0,0,1,0,0,0,1,1},
            {1,1,0,1,1,1,1,1,1,0,0,0,0,1,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
            {1,1,1,1,1,1,0,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}

    };
    public static int [] [] archerStartPositions = {  };
    public static int [] [] mageStartPositions = {
            {9},
            {6},
            {1}
    };
    public static int [] [] freezemageStartPositions = {
            {13},
            {14},
            {1}
    };
    public static int [] [] bossFreezeMageStartPositions = {
            {},
            {},
            {}
    };
    public static int [] [] bossArcherStartPositions = {};
    public static int [] [] bossMageStartPositions = {
            {},
            {},
            {}
    };
    public static int [] [] demonKingStartPositions = {
            {},
            {},
            {}
    };
    public static int [][] trueDemonKingStartPositions = {
            {},
            {},
            {}
    };
    public static int [] [] fireballStartPositions = {  {13,2,12,1}, //координата X
                                                        {13,9,1 ,2}, //Координата Y
                                                        {1, 0,2, 3}};//Направление
    public static int [] [] arrowStartPositions = { {13,11,1,3,5, 6, 7, 8},
                                                    {12,1 ,3,9,10,10,10,10},
                                                    {1, 2, 3,0,0, 0, 0, 0} };
    public static int [] infoenemy = {4,8,0,0,0,0,0,0,0,0,0,0}; //количество фаерболов, стрел, лучников, магов, ледяных магов, камикадзе, босс лучник, мишень, босс маг, лич

    public static int [] playerStartPosition= {2,11,2};

    public static int [] autosave = {9,1};

    public static int [] nextLevel = {9,8};

    void init(Context context) { // сжимаем картинку до нужных размеров
        Bitmap cBitmap = BitmapFactory.decodeResource(context.getResources(), bitmapId);
        bitmapMap = Bitmap.createScaledBitmap(
                cBitmap, (int)(GameView.unitX*GameView.cellX), (int)(GameView.unitY*GameView.cellY), false);
    }


    public Map2 (Context context){

    }
    public void equate(){
        currentMap.bitmapId=this.bitmapId;
        currentMap.map=this.map;
        currentMap.archerStartPositions=this.archerStartPositions;
        currentMap.mageStartPositions=this.mageStartPositions;
        currentMap.freezemageStartPositions=this.freezemageStartPositions;
        currentMap.bossArcherStartPositions=this.bossArcherStartPositions;
        currentMap.bossMageStartPositions=this.bossMageStartPositions;
        currentMap.bossFreezeMageStartPositions=this.bossFreezeMageStartPositions;
        currentMap.demonKingStartPositions=this.demonKingStartPositions;
        currentMap.trueDemonKingStartPositions=this.trueDemonKingStartPositions;
        currentMap.fireballStartPositions=this.fireballStartPositions;
        currentMap.arrowStartPositions=this.arrowStartPositions;
        currentMap.infoenemy=this.infoenemy;
        currentMap.playerStartPosition=this.playerStartPosition;
        currentMap.nextLevel=this.nextLevel;
        currentMap.autosave=this.autosave;

    }


    void drow(Paint paint, Canvas canvas){ // рисуем картинку
        canvas.drawBitmap(bitmapMap, 0, 0, paint);
    }
}
