package com.example.souls;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;

import com.example.souls.MapPool.Map0;
import com.example.souls.MapPool.Map3;
import com.example.souls.MapPool.Map4;
import com.example.souls.MapPool.Map5;
import com.example.souls.MapPool.Map6;
import com.example.souls.MapPool.Map7;
import com.example.souls.MapPool.Map8;
import com.example.souls.MapPool.Map9;
import com.example.souls.Projectiles.Arrow;
import com.example.souls.Projectiles.FireBall;
import com.example.souls.Projectiles.FrostBolt;
import com.example.souls.Projectiles.Projectile;
import com.example.souls.Units.BossArcher;
import com.example.souls.Units.BossFreezeMage;
import com.example.souls.Units.BossFreezeMageClone;
import com.example.souls.Units.BossMage;
import com.example.souls.Units.DemonKing;
import com.example.souls.Units.EnemyArcher;
import com.example.souls.Units.EnemyMage;
import com.example.souls.Units.FreezeMage;
import com.example.souls.Units.Pilon;
import com.example.souls.Units.Player;
import com.example.souls.Units.Target;
import com.example.souls.Units.TrueDemonKing;
import com.example.souls.Units.Unit;
import com.example.souls.MapPool.CurrentMap;
import com.example.souls.MapPool.Map1;
import com.example.souls.MapPool.Map2;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static com.example.souls.ClassSelection.picked_class;
import static com.example.souls.Game.BossHpBar;
import static com.example.souls.Game.SetBossHp;
import static com.example.souls.Game.SetPlayersHp;
import static com.example.souls.Game.StopThread;
import static com.example.souls.Game.changeIcons;
import static com.example.souls.Game.isAttackPressed;
import static com.example.souls.Game.isSpellPressed;
import static com.example.souls.MainActivity.TAG;

public class GameView extends SurfaceView implements Runnable {

    public static final int upDirection = 0;
    public static final int leftDirection = 1;
    public static final int downDirection = 2;
    public static final int rightDirection = 3;
    public final int costMovementPlain=1;
    public final int projectile_id_arrow=1;
    public final int projectile_id_fireball =2;
    public final int unit_id_player=1;
    public final int unit_id_archer=2;
    public final int unit_id_mage=3;
    public final int unit_id_freezemage=4;
    public final int unit_id_bossarcher=6;
    public final int unit_id_target=7;
    public final int unit_id_bossmage=8;
    public final int unit_id_bossfreezemage=9;
    public final int unit_id_demonking=10;
    public final int unit_id_truedemonking=11;
    public final int unit_id_pilon=12;

    public final int unit_id_bossfreezemageclone=30;

    private final int player_archer_ability_cooldown=125;
    private final int player_mage_ability_cooldown=80;

    private final int ranged_enemy_cooldown_attack = 50;
    private final int boss_aoe_attack_cooldown=50;

    private int boss_fellblast_cooldown=150;
    private int boss_fellblast_prepearing=75;

    private int boss_frostnova_cooldown=250;
    private int boss_frostnova_prepearing = 90;
    private int boss_illusions_cooldown = 400;
    private int boss_illusions_count = 2;
    private int demonking_reinforcement_cooldown=400;
    private int demonking_firestorm_cooldown=500;
    private int pilon_overcharge_cooldown =150;
    private int inferno_cooldown = 200;

    public final static int player_type_archer = 1;
    public final static int player_type_mage = 2;
    public final static int player_type_assassin =3;

    public static int cellX = 15;  //количество клеток на экране
    public static int cellY = 16;  //количество клеток на экране
    public static float unitX = 0; //количество пикселей на клетку
    public static float unitY = 0; //количество пикселей на клетку
    private boolean firstTime = true; //проверка на первый шаг

    private Player player;

    public static CurrentMap currentMap;
    public static int mapnumber=0;
    public  static int bossHp;
    private boolean bossAlive = true;

    private FireBall fireBall;
    private Arrow arrow;
    private FireBall [] fireballMas ={}; //массив фаерболов с турелей
    private Arrow[] arrowMas = {};       //массив стрел с турелей
    private ArrayList <EnemyArcher> ArcherList = new ArrayList<>();
    private ArrayList <EnemyMage> MageList = new ArrayList<>();
    private ArrayList <FreezeMage> FreezeMageList = new ArrayList<>();
    private ArrayList <BossArcher> BossArcherList = new ArrayList<>();
    private ArrayList <BossMage> BossMageList = new ArrayList<>();
    private ArrayList <BossFreezeMage> BossFreezeMageList = new ArrayList<>();
    private ArrayList <BossFreezeMageClone> BossFreezeMageCloneList = new ArrayList<>();
    private ArrayList <DemonKing> DemonKingList = new ArrayList<>();
    private ArrayList <TrueDemonKing> TrueDemonKingList = new ArrayList<>();
    private ArrayList <Target> TargetList = new ArrayList<>();
    private ArrayList <Pilon> PilonList = new ArrayList<>();

    private ArrayList <Arrow> PlayerArrowList = new ArrayList<>(); //массив стрел игрока
    private ArrayList <FireBall> PlayerFireballList = new ArrayList<>(); //массив фаерболов игрока

    private ArrayList <Arrow> ArrowList = new ArrayList<>(); //лист вражеских стрел
    private ArrayList <FireBall> FireballList = new ArrayList<>(); //лист вражеских фаерболов
    private ArrayList <FrostBolt> FrostBoltList = new ArrayList<>();

    private ArrayList <BloodStain> bloodStainList = new ArrayList<>(); //лист пятен крови
    private ArrayList <ArrayList<Integer>> fellBlastSpotsList = new ArrayList<>();
    private ArrayList <ArrayList<Integer>> frostNovaSpots = new ArrayList<>();

    public Thread gameThread = null;
    private Paint paint;
    private Canvas canvas;

    //время для основного цикла
    private Timer timer;
    private TimerTask program;

    private int tick=0;                                 //количество тиков программы
    private boolean player_no_cooldown_movement = true;    //флажок есть ли кулдаун на перемещение у игрока
    private boolean player_no_cooldown_attack = true;      //флажок есть ли кулдлаун на атаку у игрока
    private boolean player_no_cooldown_spell = true;       //флажок есть ли кулдлаун на способность у игрока

    private int player_cooldown_movement_tick;          //на каком тике произлшло движение игрока
    private int player_cooldown_attack_tick;            //на каком тике игрок атаковал
    private int player_cooldown_spell_tick=0;             //на каком тике игрок применил способность

    private boolean player_cooldown_invulnerability =false;
    private int player_cooldown_invulnerability_tick;   //тик на котором был получен урон
    private int cooldown_invulnerability = 20;          //кулдаун на неуязвимость при получении урона
    private int player_slow_tick;
    private boolean player_slowed=false;
    private boolean player_died=false; //умер ли игрок на уровне

    private int level_7_start=-2000;
    private int count_reinforcements;

    private int blink_animation_starts;
    private int blink_animation_x;
    private int blink_animation_y;

    private boolean spell_casted = false;  //был ли использован мультишот
    private boolean autosave = false;         //было ли автосохранение на уровне
    private boolean triggered = false;
    private boolean bossSecondPhase=false;
    private boolean demon_isdead = false;


    private boolean reinforcementSummoned=false;

    private Context context;

    private SurfaceHolder surfaceHolder;
    Game gameClass = new Game();
    public GameView(Context context) {

        super(context);
        this.context = context;
        //инициализируем обьекты для рисования
        surfaceHolder = getHolder();
        paint = new Paint();

        // инициализируем поток
        gameThread = new Thread(this);
        gameThread.start();

    }



    @Override
    public void run() {

         timer = new Timer();
         program = new TimerTask() {
            @Override
            public void run() {

                if (!firstTime) {
                    //перемещение игрока
                    if (player_no_cooldown_movement) {                  //если у игрока нет кулдауна на пермещение
                        if(player.update()) {                           //вызывается функция перемещения, если игрок переместился, возврат 1
                            player_no_cooldown_movement = false;        //у игрока появляется кулдаун на перемещение
                            player_cooldown_movement_tick = tick;       //записывается тик, на котором совершено перемещение

                        }
                    }
                    if(mapnumber==3 && player.x==2 && player.y==14) //невидимая стена
                        player.x=1;


                    //атака игрока
                    if (player_no_cooldown_attack && isAttackPressed){  //если у игрока нет кулдауна на атаку и нажата кнопка атаки
                        playerAttack();                                 //вызов функции атаки
                        player_no_cooldown_attack = false;              //у игрока появляется кулдаун на перемещение
                        player_cooldown_attack_tick = tick;             //записывется тик на котором совершена атака

                    }

                    //применение способности игрока
                    if (player_no_cooldown_spell && isSpellPressed){
                        //временно удалено
                        /*if(player.type==player_type_assassin){
                            blink_animation_starts=tick;
                            blink_animation_x=player.x;
                            blink_animation_y=player.y;
                        }*/
                        playerCastSpell();
                        spell_casted =true;
                        player_no_cooldown_spell=false;
                        player_cooldown_spell_tick=tick;


                    }

                    //дополнительные выстрелы от способности лучника
                    if(player.type==player_type_archer && spell_casted && (player_cooldown_spell_tick+9==tick || player_cooldown_spell_tick+17==tick)){
                        playerCastSpell();
                    }

                    //кулдаун на неуязвимость при получении урона
                    if(tick> player_cooldown_invulnerability_tick + cooldown_invulnerability){
                        player_cooldown_invulnerability =true;
                    }

                    //замедление от ледяных стрел
                    if(player_slowed && player_slow_tick+100==tick){
                        player.speed=7;
                        player_slowed=false;
                    }

                    //расчет кулдауна на перемещение
                    if (tick > player_cooldown_movement_tick + player.speed) {
                        player_no_cooldown_movement= true;

                    }
                    //рассчет кулдауна на атаку
                    if (tick > player_cooldown_attack_tick + 50) {
                        player_no_cooldown_attack = true;

                    }

                    //рассчет кулдауна на заклинание
                    switch (player.type){
                        case player_type_archer:
                            if (tick > player_cooldown_spell_tick + player_archer_ability_cooldown) {
                                player_no_cooldown_spell = true;
                            }
                            break;
                        case player_type_mage:
                            if (tick > player_cooldown_spell_tick + player_mage_ability_cooldown) {
                                player_no_cooldown_spell = true;
                            }
                            break;
                        case player_type_assassin:
                            if (tick > player_cooldown_spell_tick + 60) {
                                player_no_cooldown_spell = true;
                            }
                            break;
                    }

                    //перерасчет позиций фаерболов
                    for (int i = 0; i < fireballMas.length; i++) {
                        if (fireballMas[i] != null) {
                            if (tick % fireballMas[i].speed == 0) {
                                if(fireballMas[i].update()){
                                    fireballMas[i]=null;
                                }
                            }
                        }
                    }
                    //создание новых фаерболов
                    if(mapnumber==6 && triggered){
                        for (int i = 0; i < fireballMas.length-4; i++) {
                            if(fireballMas[i]==null){
                                createFireball(i);
                            }
                        }
                    }
                    else {
                        for (int i = 0; i < fireballMas.length; i++) {
                            if(fireballMas[i]==null){
                                createFireball(i);
                            }
                        }
                    }


                    //перерасчет позиций стрел
                    for (int i = 0; i < arrowMas.length; i++) {
                        if (arrowMas[i] != null) {
                            if (tick % arrowMas[i].speed == 0) {
                                if(arrowMas[i].update()){     //возвращает 1, если врезается в стену
                                    arrowMas[i]=null;
                                }
                            }
                        }
                    }
                    //создание новых стрел
                    for (int i = 0; i < arrowMas.length; i++) {
                        if(arrowMas[i]==null){
                            createArrow(i);
                        }
                    }
                    //перерасчет позиций вражеских стрел
                    projectilesUpdate(ArrowList);

                    //перерасчет позиций стрел игрока
                    projectilesUpdate(PlayerArrowList);

                    //перерасчет позиций ледяных стрел
                    projectilesUpdate(FrostBoltList);

                    //перерасчет позиций фаерболов игрока
                    projectilesUpdate(PlayerFireballList);

                    //перерасчет позиций вражеских фаерболов
                    projectilesUpdate(FireballList);

                    //урон игроку от фаерболов турелей
                    for (int i = 0; i < fireballMas.length; i++) {
                        if (fireballMas[i] != null) {
                            if(player.x==fireballMas[i].x&&player.y==fireballMas[i].y && player_cooldown_invulnerability){
                                player.hitpoints-=50;
                                player_cooldown_invulnerability =false;
                                player_cooldown_invulnerability_tick =tick;
                            }
                        }
                    }

                    //урон игроку от стрел турелей
                    for (int i = 0; i < arrowMas.length; i++) {
                        if (arrowMas[i] != null) {
                            if(player.x==arrowMas[i].x&&player.y==arrowMas[i].y && player_cooldown_invulnerability){
                                player.hitpoints-=40;
                                player_cooldown_invulnerability =false;
                                player_cooldown_invulnerability_tick =tick;
                            }
                        }
                    }
                    //урон игроку от вражеских стрел
                    playerTakeDamage(ArrowList);

                    //урон игроку от вражеских фаерболов
                    playerTakeDamage(FireballList);

                    //урон игроку от вражеских ледяных стрел
                    playerTakeDamage(FrostBoltList);

                    if(BossMageList.size()!=0){
                        if(BossMageList.get(0).spell_tick+boss_fellblast_prepearing==tick){
                            for(int i=0;i<fellBlastSpotsList.size();i++){
                                if (player.x==fellBlastSpotsList.get(i).get(0)&& player.y==fellBlastSpotsList.get(i).get(1)){
                                    player.hitpoints-=70;
                                }

                            }
                        }
                    }

                    if(BossFreezeMageList.size()!=0){
                        if(BossFreezeMageList.get(0).spell_tick+boss_frostnova_prepearing==tick){
                            for(int i=0;i<frostNovaSpots.size();i++){
                                if (player.x==frostNovaSpots.get(i).get(0)&& player.y==frostNovaSpots.get(i).get(1)){
                                    player.hitpoints-=70;
                                }
                            }
                        }
                    }

                    if(TrueDemonKingList.size()!=0){
                        if(TrueDemonKingList.get(0).spell2_tick+boss_frostnova_prepearing==tick){
                            for(int i=0;i<frostNovaSpots.size();i++){
                                if (player.x==frostNovaSpots.get(i).get(0)&& player.y==frostNovaSpots.get(i).get(1)){
                                    player.hitpoints-=70;
                                }
                            }
                        }
                    }

                    //получение урона лучниками
                    enemyTakeDamage(ArcherList,unit_id_archer, PlayerArrowList, PlayerFireballList);

                    //получение урона магами
                    enemyTakeDamage(MageList, unit_id_mage, PlayerArrowList, PlayerFireballList);

                    //получение урона ледяными магами
                    enemyTakeDamage(FreezeMageList,unit_id_freezemage, PlayerArrowList, PlayerFireballList);

                    //получение урона боссом лучником
                    enemyTakeDamage(BossArcherList,unit_id_bossarcher,PlayerArrowList,PlayerFireballList);

                    //получение урона боссом магом
                    enemyTakeDamage(BossMageList,unit_id_bossmage,PlayerArrowList,PlayerFireballList);

                    //получение урона боссом ледяным магом
                    enemyTakeDamage(BossFreezeMageList,unit_id_bossfreezemage,PlayerArrowList,PlayerFireballList);

                    //получение урона королем демонов
                    enemyTakeDamage(DemonKingList,unit_id_demonking,PlayerArrowList,PlayerFireballList);

                    //получение урона истинным королем демонов
                    enemyTakeDamage(TrueDemonKingList,unit_id_truedemonking,PlayerArrowList,PlayerFireballList);

                    //получение урона иллизиями босса ледяного мага
                    enemyTakeDamage(BossFreezeMageCloneList,unit_id_bossfreezemageclone,PlayerArrowList,PlayerFireballList);

                    //получение урона мишенью
                    enemyTakeDamage(TargetList,unit_id_target,PlayerArrowList,PlayerFireballList);

                    //получение урона пилонами
                    enemyTakeDamage(PilonList,unit_id_pilon,PlayerArrowList,PlayerFireballList);

                    CheckTrigger(); //проверка всех триггеров

                    //подкрепления на 7 карте
                    if(mapnumber==7)
                        TimingReinforcement();


                    //перемещение лучников
                    for(int i=0;i<ArcherList.size();i++) {
                        rangedEnemyMoving(ArcherList.get(i), unit_id_archer,ArrowList);
                    }

                    //перемещение магов
                    for(int i=0;i<MageList.size();i++) {
                        rangedEnemyMoving(MageList.get(i), unit_id_mage,FireballList);
                    }
                    //перемещение ледяных магов
                    for(int i=0;i<FreezeMageList.size();i++) {
                        rangedEnemyMoving(FreezeMageList.get(i), unit_id_freezemage ,FrostBoltList);
                    }
                    //перемещение босса лучника
                    for(int i=0;i<BossArcherList.size();i++) {
                        bossMoving(BossArcherList.get(i), unit_id_bossarcher ,ArrowList);
                    }

                    //перемещение босса мага
                    for(int i=0;i<BossMageList.size();i++) {
                        bossMoving(BossMageList.get(i), unit_id_bossmage ,FireballList);
                    }

                    //перемещение босса ледяного мага
                    for(int i=0;i<BossFreezeMageList.size();i++) {
                        bossMoving(BossFreezeMageList.get(i), unit_id_bossfreezemage ,FrostBoltList);
                    }

                    //перемещение иллюзий босса ледяного мага
                    for(int i=0;i<BossFreezeMageCloneList.size();i++) {
                        rangedEnemyMoving(BossFreezeMageCloneList.get(i), unit_id_bossfreezemageclone ,FrostBoltList);
                    }

                    //перемещение короля демонов
                    for(int i=0;i<DemonKingList.size();i++) {
                        bossMoving(DemonKingList.get(i), unit_id_demonking ,FireballList);
                    }

                    //перемещение истинного короля демонов
                    for(int i=0;i<TrueDemonKingList.size();i++) {
                        bossMoving(TrueDemonKingList.get(i), unit_id_truedemonking ,FireballList);
                    }

                    //перемещение мишени
                    for(int i=0;i<TargetList.size();i++) {
                        if(tick%TargetList.get(i).speed==0)
                            TargetList.get(i).update();
                    }

                    if(!bossSecondPhase)
                        BossSecondStage();


                    //смерть иллюзий босса фризмага при его смерти
                    if(BossFreezeMageList.size()==0)
                        BossFreezeMageCloneList.clear();


                    if(mapnumber==8 & DemonKingList.size()==0 && !demon_isdead){
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Toast demon_defeat = Toast.makeText(context,R.string.demonking_defeat, Toast.LENGTH_SHORT);
                                demon_defeat.show();
                            }
                        });
                        demon_isdead=true;
                        MageList.clear();
                        ArcherList.clear();
                    }

                    //смерть игрока
                    if(player.hitpoints<=0){

                        BloodStain bloodStain = new BloodStain(getContext());
                        bloodStain.x=player.x;
                        bloodStain.y=player.y;
                        bloodStainList.add(bloodStain);
                        firstTime=true;
                        player_died=true;
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Toast death = Toast.makeText(context,R.string.death, Toast.LENGTH_SHORT);
                                death.show();
                            }
                        });



                    }
                    //автосохранение
                    if(currentMap.autosave[0]==player.x && currentMap.autosave[1]==player.y && !autosave){
                        autosave=true;
                        currentMap.playerStartPosition[0]=currentMap.autosave[0];
                        currentMap.playerStartPosition[1]=currentMap.autosave[1];
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Toast autoSaveToast = Toast.makeText(context,R.string.autosave, Toast.LENGTH_LONG);
                                autoSaveToast.show();
                            }
                        });
                    }

                    //переход на следующий уровень
                    if(player.x == currentMap.nextLevel[0] && player.y == currentMap.nextLevel[1] && ArcherList.size()==0 && MageList.size()==0 && FreezeMageList.size()==0 && BossArcherList.size()==0
                    &&BossMageList.size()==0 && BossFreezeMageList.size()==0 && mapnumber!=7 && DemonKingList.size()==0)
                    {
                        mapnumber++;
                        firstTime=true;
                        bloodStainList.clear();
                        player_died=false;
                    }
                    if(ArcherList.size()==0 && MageList.size()==0 && player.x == currentMap.nextLevel[0] && player.y == currentMap.nextLevel[1] && mapnumber==7 && count_reinforcements==2)
                    {
                        mapnumber++;
                        firstTime=true;
                        bloodStainList.clear();
                        player_died=false;
                    }
                    playerChangeModel(); //изменение модели игрока при повороте
                }
                drow();
                tick++;

                if(StopThread){
                    program.cancel();
                    gameThread.interrupt();
                    Log.d(TAG,"Поток остановлен" );
                }
            }

        };

      timer.schedule(program,0,20);
    }


    private void drow(){
        if (surfaceHolder.getSurface().isValid()) {
            if(firstTime){
                firstTime = false;
                if(!player_died) //если игрок перешел на следующий уровень, а не умер, отключение автосохранения
                    autosave=false;
                PlayerFireballList.clear();
                PlayerArrowList.clear();
                ArcherList.clear();
                MageList.clear();
                FreezeMageList.clear();
                FrostBoltList.clear();
                ArrowList.clear();
                FireballList.clear();
                BossArcherList.clear();
                BossFreezeMageList.clear();
                BossFreezeMageCloneList.clear();
                BossMageList.clear();
                TargetList.clear();
                DemonKingList.clear();
                TrueDemonKingList.clear();
                PilonList.clear();
                reinforcementSummoned=false;
                triggered =false;
                bossSecondPhase=false;
                demon_isdead=false;
                //восстановление параметров боссов
                boss_fellblast_cooldown=150;
                boss_fellblast_prepearing=75;

                boss_illusions_count=2;
                boss_illusions_cooldown=400;

                unitX = surfaceHolder.getSurfaceFrame().width()/cellX; // вычисляем число пикселей в юните
                unitY = surfaceHolder.getSurfaceFrame().height()/cellY; // вычисляем число пикселей в юните
                player = new Player(getContext());
                currentMap.map=null;
                currentMap = new CurrentMap(getContext());
                switch (mapnumber){
                    case 0:
                            Map0 map0 = new Map0(getContext());
                            map0.equate();
                            currentMap.init(getContext());
                            break;
                    case 1:
                            Map1 map1 = new Map1(getContext());
                            map1.equate();
                            currentMap.init(getContext());
                            break;
                    case 2:
                            Map2 map2 = new Map2(getContext());
                            map2.equate();
                            currentMap.init(getContext());
                            break;
                    case 3:
                            Map3 map3 = new Map3(getContext());
                            map3.equate();
                            currentMap.init(getContext());
                            break;
                    case 4:
                            Map4 map4 = new Map4(getContext());
                            map4.equate();
                            currentMap.init(getContext());
                            break;
                    case 5:
                            Map5 map5 = new Map5(getContext());
                            map5.equate();
                            currentMap.init(getContext());
                            break;
                    case 6:
                            Map6 map6 = new Map6(getContext());
                            map6.equate();
                            currentMap.init(getContext());
                            break;
                    case 7:
                            Map7 map7 = new Map7(getContext());
                            map7.equate();
                            currentMap.init(getContext());
                            break;
                    case 8:
                            Map8 map8 = new Map8(getContext());
                            map8.equate();
                            currentMap.init(getContext());
                            break;
                    case 9:
                            Map9 map9 = new Map9(getContext());
                            map9.equate();
                            currentMap.init(getContext());
                            break;
                }

                if(mapnumber==7){
                    level_7_start=tick;
                    count_reinforcements=0;
                }

                if(mapnumber==8&&!player_died){ //приветствие короля демонов
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            int line = (int) (Math.random()*3);
                            switch (line){
                                case 0:
                                    Toast line0 = Toast.makeText(context,R.string.demonking_greeting1, Toast.LENGTH_SHORT);
                                    line0.show();
                                    break;
                                case 1:
                                    Toast line1 = Toast.makeText(context,R.string.demonking_greeting2, Toast.LENGTH_SHORT);
                                    line1.show();
                                    break;
                                case 2:
                                    if(player.type==player_type_archer){
                                        Toast line2 = Toast.makeText(context,R.string.demonking_greeting3_archer, Toast.LENGTH_SHORT);
                                        line2.show();
                                    }
                                    else {
                                        Toast line2 = Toast.makeText(context,R.string.demonking_greeting3_mage, Toast.LENGTH_SHORT);
                                        line2.show();
                                    }
                                    break;
                            }
                        }
                    });
                }
                createElems();  //создание всех элемеентов
                changeIcons(player.type); //изменяются иконки в зависимости от типа персонажа (функция в Game.java)

                if(BossArcherList.size()!=0 || BossMageList.size()!=0 || BossFreezeMageList.size()!=0 || DemonKingList.size()!=0 || TrueDemonKingList.size()!=0){
                    bossAlive=true;
                }
                else {
                    bossAlive=false;
                }

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if(BossArcherList.size()!=0) { //хп бар для всех боссов
                            bossHp = BossArcherList.get(0).hitpoints;
                            BossHpBar.setText(bossHp + "HP");
                            BossHpBar.setVisibility(View.VISIBLE);
                        }
                        else if(BossMageList.size()!=0){
                            bossHp = BossMageList.get(0).hitpoints;
                            BossHpBar.setText(bossHp + "HP");
                            BossHpBar.setVisibility(View.VISIBLE);
                        }
                        else if(BossFreezeMageList.size()!=0){
                            bossHp = BossFreezeMageList.get(0).hitpoints;
                            BossHpBar.setText(bossHp + "HP");
                            BossHpBar.setVisibility(View.VISIBLE);
                        }
                        else if(DemonKingList.size()!=0){
                            bossHp = DemonKingList.get(0).hitpoints;
                            BossHpBar.setText(bossHp + "HP");
                            BossHpBar.setVisibility(View.VISIBLE);
                        }
                        else{
                            BossHpBar.setVisibility(View.GONE);
                        }
                    }
                });
            }


            if(BossArcherList.size()!=0) {
                bossHp = BossArcherList.get(0).hitpoints;
                SetBossHp(bossHp);
            }

            if(BossMageList.size()!=0) {
                bossHp = BossMageList.get(0).hitpoints;
                SetBossHp(bossHp);
            }

            if(BossFreezeMageList.size()!=0) {
                bossHp = BossFreezeMageList.get(0).hitpoints;
                SetBossHp(bossHp);
            }
            if(DemonKingList.size()!=0) {
                bossHp = DemonKingList.get(0).hitpoints;
                SetBossHp(bossHp);
            }

            if(TrueDemonKingList.size()!=0) {
                bossHp = TrueDemonKingList.get(0).hitpoints;
                Log.d(TAG,Integer.toString(TrueDemonKingList.get(0).hitpoints));
                SetBossHp(bossHp);
            }


            if (bossAlive && BossArcherList.size()==0 && BossMageList.size()==0 && BossFreezeMageList.size()==0 && DemonKingList.size()==0 && TrueDemonKingList.size()==0){
                bossAlive=false;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        BossHpBar.setVisibility(View.GONE);
                    }
                });
            }


            canvas = surfaceHolder.lockCanvas(); // закрываем canvas
            currentMap.drow(paint,canvas); //рисуем карту

            for(int i=0;i<bloodStainList.size();i++){  //отрисовка пятен крови
                bloodStainList.get(i).drow(paint,canvas);
            }
            //анимация блинка ассассина
            /*if(blink_animation_starts+50>tick && spell_casted){
                blinkAnimationDrow(blink_animation_x,blink_animation_y);
            }*/
            //анимация взрывов скверны
            if(BossMageList.size()!=0){
                if(BossMageList.get(0).spell_tick+boss_fellblast_prepearing>tick){
                    for(int i=0;i<fellBlastSpotsList.size();i++){
                        fellBlastDrow(fellBlastSpotsList.get(i).get(0),fellBlastSpotsList.get(i).get(1));
                    }
                }
            }

            //анимация ледяного взрыва
            if(BossFreezeMageList.size()!=0){
                if(BossFreezeMageList.get(0).spell_tick+boss_frostnova_prepearing>tick){
                    for(int i=0;i<frostNovaSpots.size();i++){
                        frostNovaDrow(frostNovaSpots.get(i).get(0),frostNovaSpots.get(i).get(1));
                    }
                }
            }

            //анимация инферно
            if(TrueDemonKingList.size()!=0){
                if(TrueDemonKingList.get(0).spell2_tick+boss_frostnova_prepearing>tick){
                    for(int i=0;i<frostNovaSpots.size();i++){
                        frostNovaDrow(frostNovaSpots.get(i).get(0),frostNovaSpots.get(i).get(1));
                    }
                }
            }

            player.drow(paint, canvas);     //рисуем игрока

            SetPlayersHp(Player.hitpoints); //отображение здоровья в интерфейсе

            //отрисовкка лучников
            for(int i=0;i<ArcherList.size();i++){
                    ArcherList.get(i).drow(paint,canvas);
            }
            //отрисовка босса лучника
            for(int i=0;i<BossArcherList.size();i++){
                BossArcherList.get(i).drow(paint,canvas);
            }
            //отрисовкка магов
            for(int i=0;i<MageList.size();i++){
                MageList.get(i).drow(paint,canvas);
            }

            //отрисовка босса мага
            for(int i=0;i<BossMageList.size();i++){
                BossMageList.get(i).drow(paint,canvas);
            }

            //отрисовка босса ледяного мага
            for(int i=0;i<BossFreezeMageList.size();i++){
                BossFreezeMageList.get(i).drow(paint,canvas);
            }

            //отрисовка клонов босса ледяного мага
            for(int i=0;i<BossFreezeMageCloneList.size();i++){
                BossFreezeMageCloneList.get(i).drow(paint,canvas);
            }

            //отрисовка короля демонов
            for(int i=0;i<DemonKingList.size();i++){
                DemonKingList.get(i).drow(paint,canvas);
            }

            //отрисовка истинного короля демонов
            for(int i=0;i<TrueDemonKingList.size();i++){
                TrueDemonKingList.get(i).drow(paint,canvas);
            }


            //отрисовка ледяных магов
            for(int i=0;i<FreezeMageList.size();i++){
                FreezeMageList.get(i).drow(paint,canvas);
            }

            //отрисовка мишени
            for(int i=0;i<TargetList.size();i++){
                TargetList.get(i).drow(paint,canvas);
            }

            //отрисовка пилонов
            for(int i=0;i<PilonList.size();i++){
                PilonList.get(i).drow(paint,canvas);
            }

            //отрисовка вражеских стрел
            for(int i=0;i<ArrowList.size();i++){
                ArrowList.get(i).drow(paint,canvas);
            }

            for(int i=0;i<FireballList.size();i++){
                FireballList.get(i).drow(paint,canvas);
            }

            for(int i=0;i<FrostBoltList.size();i++){
                FrostBoltList.get(i).drow(paint,canvas);
            }

            //отрисовка фаерболов из турелей
            for(int i=0;i<fireballMas.length;i++) {
                if (fireballMas[i] != null)
                    fireballMas[i].drow(paint, canvas);
            }
            //отрисовка стрел из турелей
            for(int i=0;i<arrowMas.length;i++) {
                if (arrowMas[i] != null)
                    arrowMas[i].drow(paint, canvas);
            }
            //отрисовка фаерболов игрока
            for(int i=0;i<PlayerArrowList.size();i++) {
                PlayerArrowList.get(i).drow(paint, canvas);
            }
            //отрисовка стрел игрока
            for(int i=0;i<PlayerFireballList.size();i++) {
                PlayerFireballList.get(i).drow(paint, canvas);
            }


            surfaceHolder.unlockCanvasAndPost(canvas); // открываем canvas
        }
        }

    private void createElems(){
        //начальные координаты игрока
        player.x=currentMap.playerStartPosition[0];
        player.y=currentMap.playerStartPosition[1];
        player.direction=currentMap.playerStartPosition[2];
        player.hitpoints=player.hitpoints + 10 * mapnumber;

        //выбор класса
        switch (picked_class){
            case 1:
                player.type=player_type_archer;
                break;
            case 2:
                player.type=player_type_mage;
                break;
            case 3:
                player.type= player_type_assassin;
                break;
        }

        //определение размеров массивов с фаерболами и стрелами
        fireballMas = new FireBall[currentMap.infoenemy[0]];
        arrowMas = new Arrow[currentMap.infoenemy[1]];

        //создание фаерболов
        for(int i=0;i<currentMap.infoenemy[0];i++){
            createFireball(i);
        }

        //создание стрел
        for(int i=0; i<currentMap.infoenemy[1];i++){
            createArrow(i);
        }

        //создание лучников
        for(int i=0; i<currentMap.infoenemy[2];i++){
            createEnemyUnit(unit_id_archer, i ,ArcherList);
        }

        //создание магов
        for(int i=0; i<currentMap.infoenemy[3];i++){
            createEnemyUnit(unit_id_mage, i ,MageList);
        }

        //создание ледяных магов
        for(int i=0; i<currentMap.infoenemy[4];i++){
            createEnemyUnit(unit_id_freezemage, i ,FreezeMageList);
        }
        //создание босса лучника
        for(int i=0; i<currentMap.infoenemy[6];i++){
            createEnemyUnit(unit_id_bossarcher, i ,BossArcherList);
        }
        //создание мишени
        for(int i=0; i<currentMap.infoenemy[unit_id_target];i++){
            createEnemyUnit(unit_id_target, i ,TargetList);
        }
        //создание босса мага
        for(int i=0; i<currentMap.infoenemy[unit_id_bossmage];i++){
            createEnemyUnit(unit_id_bossmage, i ,BossMageList);
        }
        //создание босса ледяного мага
        for(int i=0; i<currentMap.infoenemy[unit_id_bossfreezemage];i++){
            createEnemyUnit(unit_id_bossfreezemage, i ,BossFreezeMageList);
        }
        //создание короля демонов
        for(int i=0; i<currentMap.infoenemy[unit_id_demonking];i++){
            createEnemyUnit(unit_id_demonking, i ,DemonKingList);
        }
        //создание истинного короля демонов
        for(int i=0; i<currentMap.infoenemy[unit_id_truedemonking];i++){
            createEnemyUnit(unit_id_truedemonking, i ,TrueDemonKingList);
        }

    }

    private void createFireball(int number){
        fireBall = new FireBall(getContext());
        fireBall.x=currentMap.fireballStartPositions[0][number];
        fireBall.y=currentMap.fireballStartPositions[1][number];
        fireBall.direction=currentMap.fireballStartPositions[2][number];
        switch (fireBall.direction){
            case leftDirection:
                fireBall.setLeftModel(getContext());
                break;
            case downDirection:
                fireBall.setDownModel(getContext());
                break;
            case rightDirection:
                fireBall.setRightModel(getContext());
                break;
            case upDirection:
                fireBall.setUpModel(getContext());
                break;
        }

        fireballMas[number]=fireBall;
    }

    private void createArrow(int number){
        arrow = new Arrow(getContext());
        arrow.x=currentMap.arrowStartPositions[0][number];
        arrow.y=currentMap.arrowStartPositions[1][number];
        arrow.direction=currentMap.arrowStartPositions[2][number];
        switch (arrow.direction){
            case leftDirection:
                arrow.setLeftModel(getContext());
                break;
            case downDirection:
                arrow.setDownModel(getContext());
                break;
            case rightDirection:
                arrow.setRightModel(getContext());
                break;
            case upDirection:
                arrow.setUpModel(getContext());
                break;
        }
        arrowMas[number]=arrow;
    }

    private void createEnemyUnit(int unit_type, int number, ArrayList list){
        Unit unit = null;
        ArrayList <Unit> units = new ArrayList<>();
        switch (unit_type){
            case unit_id_archer:
                EnemyArcher archer = new EnemyArcher(getContext());
                unit=archer;
                if(mapnumber>5){
                    unit.hitpoints=150;
                }
                if(mapnumber==9){
                    unit.hitpoints=350; //ToDo для отладки ласт босса
                }
                break;
            case unit_id_mage:
                EnemyMage mage = new EnemyMage(getContext());
                unit=mage;
                if(mapnumber>5){
                    unit.hitpoints=150;
                }
                if(mapnumber==9){
                    unit.hitpoints=350;
                }
                break;
            case unit_id_freezemage:
                FreezeMage freezeMage = new FreezeMage(getContext());
                unit=freezeMage;
                break;
            case unit_id_bossarcher:
                BossArcher boss = new BossArcher(getContext());
                unit=boss;
                break;
            case unit_id_target:
                Target target = new Target(getContext());
                unit=target;
                break;
            case unit_id_bossmage:
                BossMage bossMage = new BossMage(getContext());
                unit=bossMage;
                break;
            case unit_id_bossfreezemage:
                BossFreezeMage bossFreezeMage = new BossFreezeMage(getContext());
                unit=bossFreezeMage;
                break;
            case unit_id_bossfreezemageclone:
                BossFreezeMageClone bossFreezeMageClone = new BossFreezeMageClone(getContext());
                unit=bossFreezeMageClone;
                break;
            case unit_id_demonking:
                DemonKing demonKing = new DemonKing(getContext());
                unit=demonKing;
                break;
            case unit_id_truedemonking:
                TrueDemonKing truedemonking = new TrueDemonKing(getContext());
                unit=truedemonking;
                break;
            case unit_id_pilon:
                Pilon pilon = new Pilon(getContext());
                unit = pilon;
        }
        units = list;
        switch (unit_type) {
            case unit_id_archer:
                unit.x = currentMap.archerStartPositions[0][number];
                unit.y = currentMap.archerStartPositions[1][number];
                unit.direction = currentMap.archerStartPositions[2][number];
            break;
            case unit_id_mage:
                unit.x = currentMap.mageStartPositions[0][number];
                unit.y = currentMap.mageStartPositions[1][number];
                unit.direction = currentMap.mageStartPositions[2][number];
                break;
            case unit_id_freezemage:
                unit.x = currentMap.freezemageStartPositions[0][number];
                unit.y = currentMap.freezemageStartPositions[1][number];
                unit.direction = currentMap.freezemageStartPositions[2][number];
                break;
            case unit_id_bossarcher:
                unit.x = currentMap.bossArcherStartPositions[0][number];
                unit.y = currentMap.bossArcherStartPositions[1][number];
                unit.direction = currentMap.bossArcherStartPositions[2][number];
                break;
            case unit_id_bossmage:
                unit.x = currentMap.bossMageStartPositions[0][number];
                unit.y = currentMap.bossMageStartPositions[1][number];
                unit.direction = currentMap.bossMageStartPositions[2][number];
                break;
            case unit_id_bossfreezemage:
                unit.x = currentMap.bossFreezeMageStartPositions[0][number];
                unit.y = currentMap.bossFreezeMageStartPositions[1][number];
                unit.direction = currentMap.bossFreezeMageStartPositions[2][number];
                break;
            case unit_id_demonking:
                unit.x = currentMap.demonKingStartPositions[0][number];
                unit.y = currentMap.demonKingStartPositions[1][number];
                unit.direction = currentMap.demonKingStartPositions[2][number];
                break;
            case unit_id_truedemonking:
                unit.x = currentMap.trueDemonKingStartPositions[0][number];
                unit.y = currentMap.trueDemonKingStartPositions[1][number];
                unit.direction = currentMap.trueDemonKingStartPositions[2][number];
                break;
            case unit_id_bossfreezemageclone:
                boolean added=false;
                while (!added) {
                    int x = 1 + (int) (Math.random() * 13); //диапазон от 1 до 14
                    int y = 1 + (int) (Math.random() * 14); //диапазон от 1 до 15
                    if (currentMap.map[y][x] != 1) {
                        unit.x=x;
                        unit.y=y;
                        unit.direction=1;
                        added=true;
                    }
                }
                break;
            case unit_id_pilon:
                boolean added_pilon=false;
                boolean blocked = false;
                while (!added_pilon) {
                    int x = 1 + (int) (Math.random() * 13); //диапазон от 1 до 14
                    int y = 1 + (int) (Math.random() * 14); //диапазон от 1 до 15
                    if (currentMap.map[y][x] != 1) {
                        Log.d(TAG,Integer.toString(PilonList.size()));
                        for(int i=0;i<PilonList.size();i++){
                            if(PilonList.get(i).x==x && PilonList.get(i).y==y){
                                blocked=true;
                            }
                        }

                        if(!blocked) {
                            unit.x = x;
                            unit.y = y;
                            unit.direction = 1;
                            added_pilon = true;
                        }
                    }
                }
                break;

        }
        switch (unit.direction){
            case leftDirection:
                unit.setLeftModel(getContext());
                break;
            case downDirection:
                unit.setDownModel(getContext());
                break;
            case rightDirection:
                unit.setRightModel(getContext());
                break;
            case upDirection:
                unit.setUpModel(getContext());
                break;
        }
        units.add(unit);

    }

    private void summonReinforcement(){
        switch (mapnumber){
            case 0:
                for(int i=3;i<5;i++){
                    createEnemyUnit(unit_id_archer, i ,ArcherList);
                }
                break;
            case 1:
                for(int i=0;i<2;i++){
                    createEnemyUnit(unit_id_archer, i ,ArcherList);
                }
                break;
            case 3:
                for(int i=0;i<3;i++){
                    createEnemyUnit(unit_id_mage, i ,MageList);
                }
                break;
            case 6:
                for(int i=0;i<2;i++){
                    createEnemyUnit(unit_id_mage, i ,MageList);
                }
                for(int i=0;i<2;i++){
                    createEnemyUnit(unit_id_freezemage, i ,FreezeMageList);
                }
                break;
            case 7:
                if(count_reinforcements==0) {
                    createEnemyUnit(unit_id_mage, 1, MageList);
                    createEnemyUnit(unit_id_archer, 1, ArcherList);
                }
                if(count_reinforcements==1){
                    createEnemyUnit(unit_id_mage, 2, MageList);
                    createEnemyUnit(unit_id_archer, 2, ArcherList);
                }
                if(count_reinforcements==2){
                    createEnemyUnit(unit_id_mage, 3, MageList);
                    createEnemyUnit(unit_id_archer, 3, ArcherList);
                }
                break;
            case 9:
                createEnemyUnit(unit_id_truedemonking,0,TrueDemonKingList );
                for(int i=0; i<4;i++){
                    createEnemyUnit(unit_id_pilon,i,PilonList);
                }
                break;

        }
        reinforcementSummoned=true;

    }

    private void CheckTrigger(){
        if(!reinforcementSummoned && mapnumber==0){
            if(player.x==9 && player.y==12){
                summonReinforcement();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Toast death = Toast.makeText(context,R.string.trap, Toast.LENGTH_SHORT);
                        death.show();
                    }
                });
            }
        }
        if(mapnumber==3 && TargetList.size()==0 && !triggered){//триггер на открытие прохода на 3 карте
            triggered = true;
            Trigger();
        }

        if(mapnumber==4 && BossMageList.size()==0 && !triggered){//триггер на открытие прохода на 4 карте
            triggered = true;
            Trigger();
        }

        if(mapnumber==6 && !triggered && player.x==8 && player.y==1){//триггер на закрытие прохода на 6 карте
            triggered = true;
            Trigger();
        }

        if(mapnumber==9 && !triggered && MageList.size()==0 && ArcherList.size()==0){//призыв истинного короля демонов
            triggered = true;
            Trigger();
        }
    }

    private void Trigger (){
        switch (mapnumber){

            case 3:
                currentMap.bitmapId=R.drawable.map3open;
                currentMap.map[6][9]=0;
                currentMap.init(getContext());
                summonReinforcement();
                break;
            case 4:
                currentMap.bitmapId=R.drawable.map4open;
                currentMap.map[6][7]=0;
                currentMap.map[8][7]=0;
                currentMap.map[7][6]=0;
                currentMap.map[7][8]=0;
                currentMap.init(getContext());
                break;
            case 6:
                currentMap.bitmapId=R.drawable.map6close;
                currentMap.map[1][9]=1;
                currentMap.init(getContext());
                summonReinforcement();
                break;
            case 9:
                summonReinforcement();
                bossAlive=true;
                if(!player_died) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            int line = (int) (Math.random() * 2);
                            switch (line) {
                                case 0:
                                    Toast line0 = Toast.makeText(context, R.string.truedemonking_greeting1, Toast.LENGTH_SHORT);
                                    line0.show();
                                    break;
                                case 1:
                                    Toast line1 = Toast.makeText(context, R.string.truedemonking_greeting2, Toast.LENGTH_SHORT);
                                    line1.show();
                                    break;

                            }
                        }
                    });
                }
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if(TrueDemonKingList.size()!=0) {
                            bossHp = TrueDemonKingList.get(0).hitpoints;
                            BossHpBar.setText(bossHp + "HP");
                            BossHpBar.setVisibility(View.VISIBLE);
                        }

                    }
                });

                break;
        }
    }

    private void TimingReinforcement (){
        if (level_7_start+700==tick ){
            summonReinforcement();
        }
        if (level_7_start+1400==tick ){
            count_reinforcements=1;
            summonReinforcement();
        }
        if (level_7_start+2100==tick ){
            count_reinforcements=2;
            summonReinforcement();
        }
    }

    private void BossSecondStage(){
        if(!reinforcementSummoned && BossArcherList.size()!=0){
            if(BossArcherList.get(0).hitpoints<=300){
                summonReinforcement();
                bossSecondPhase=true;
            }

        }

        if(!bossSecondPhase && BossMageList.size()!=0){
            if(BossMageList.get(0).hitpoints<=450){
                boss_fellblast_prepearing=50;
                boss_fellblast_cooldown=60;
                bossSecondPhase=true;
            }
        }
        if(!bossSecondPhase && BossFreezeMageList.size()!=0){
            if(BossFreezeMageList.get(0).hitpoints<=250){
                boss_illusions_cooldown=300;
                boss_illusions_count=3;
                bossSecondPhase=true;
            }
        }

    }

    private void playerChangeModel(){ //выбор модели для разных персонажей
        switch (player.type){
            case player_type_archer:
                switch (player.direction){
                    case leftDirection:
                        player.bitmapId=R.drawable.archerleft;
                        player.init(getContext());
                        break;
                    case rightDirection:
                        player.bitmapId=R.drawable.archerright;
                        player.init(getContext());
                        break;
                    case upDirection:
                        player.bitmapId=R.drawable.archertop;
                        player.init(getContext());
                        break;
                    case downDirection:
                        player.bitmapId=R.drawable.archerbot;
                        player.init(getContext());
                        break;
                }
                break;
            case player_type_mage:
                switch (player.direction){
                    case leftDirection:
                        player.bitmapId=R.drawable.mageleft;
                        player.init(getContext());
                        break;
                    case rightDirection:
                        player.bitmapId=R.drawable.mageright;
                        player.init(getContext());
                        break;
                    case upDirection:
                        player.bitmapId=R.drawable.magetop;
                        player.init(getContext());
                        break;
                    case downDirection:
                        player.bitmapId=R.drawable.magebot;
                        player.init(getContext());
                        break;
                }
                break;
        }

    }

    private void playerAttack(){
        switch (player.type){
            case player_type_archer:
                arrow = new Arrow(getContext()); //создание новой стрелы
                boolean arrowCreated=false;      //была ли стрела создана
                //в зависимости от направления персонажа создается стрела, если есть место
                switch (player.direction){
                    case downDirection:
                        if(currentMap.map[player.y+1][player.x]!=1) {
                            arrow.direction = downDirection;
                            arrow.x = player.x;
                            arrow.y = player.y + 1;
                            arrow.setDownModel(getContext());
                            arrowCreated=true;
                        }
                        break;
                    case upDirection:
                        if(currentMap.map[player.y-1][player.x]!=1) {
                            arrow.direction = upDirection;
                            arrow.x = player.x;
                            arrow.y = player.y - 1;
                            arrow.setUpModel(getContext());
                            arrow.init(getContext());
                            arrowCreated=true;
                        }
                        break;
                    case leftDirection:
                        if(currentMap.map[player.y][player.x-1]!=1) {
                            arrow.direction = leftDirection;
                            arrow.x = player.x - 1;
                            arrow.y = player.y;
                            arrow.setLeftModel(getContext());
                            arrowCreated=true;
                        }
                        break;
                    case rightDirection:
                        if(currentMap.map[player.y][player.x+1]!=1) {
                            arrow.direction = rightDirection;
                            arrow.x = player.x + 1;
                            arrow.y = player.y;
                            arrow.setRightModel(getContext());
                            arrowCreated=true;
                        }
                        break;
                }
                //если стрела была создана
                if(arrowCreated)
                    PlayerArrowList.add(arrow);
                break;

            case player_type_mage: //аналогично стрелам
                fireBall = new FireBall(getContext());
                boolean fireballCreated=false;
                switch (player.direction){
                    case downDirection:
                        if(currentMap.map[player.y+1][player.x]!=1) {
                            fireBall.direction = downDirection;
                            fireBall.x = player.x;
                            fireBall.y = player.y + 1;
                            fireBall.bitmapId= R.drawable.fireballdown;
                            fireBall.init(getContext());
                            fireballCreated=true;
                        }
                        break;
                    case upDirection:
                        if(currentMap.map[player.y-1][player.x]!=1) {
                            fireBall.direction = upDirection;
                            fireBall.x = player.x;
                            fireBall.y = player.y - 1;
                            fireBall.bitmapId= R.drawable.fireballup;
                            fireBall.init(getContext());
                            fireballCreated=true;
                        }
                        break;
                    case leftDirection:
                        if(currentMap.map[player.y][player.x-1]!=1) {
                            fireBall.direction = leftDirection;
                            fireBall.x = player.x - 1;
                            fireBall.y = player.y;
                            fireBall.bitmapId= R.drawable.fireballleft;
                            fireBall.init(getContext());
                            fireballCreated=true;
                        }
                        break;
                    case rightDirection:
                        if(currentMap.map[player.y][player.x+1]!=1) {
                            fireBall.direction = rightDirection;
                            fireBall.x = player.x + 1;
                            fireBall.y = player.y;
                            fireBall.bitmapId= R.drawable.fireballright;
                            fireBall.init(getContext());
                            fireballCreated=true;
                        }
                        break;
                }
                if(fireballCreated) {
                    PlayerFireballList.add(fireBall);
                }
                break;

            case player_type_assassin:
                    //ToDO: атака асасина
                break;

        }
    }

    private void playerCastSpell() {
        switch (player.type) {
            case player_type_archer:
                playerAttack();
                break;
            case player_type_mage:
                int oldirection = player.direction; //запись старого направления
                //атака во все стороны
                player.direction=upDirection;
                playerAttack();
                player.direction=leftDirection;
                playerAttack();
                player.direction=downDirection;
                playerAttack();
                player.direction=rightDirection;
                playerAttack();
                //возврат старого направления
                player.direction = oldirection;
                break;
            case player_type_assassin:
                switch (player.direction) {
                    case downDirection:
                    for (int i = 0; i < 3; i++) {
                        if(currentMap.map[player.y+1][player.x]!=1) {
                            player.y+=1;
                        }
                    }
                    break;
                    case upDirection:
                        for (int i = 0; i < 3; i++) {
                            if (currentMap.map[player.y - 1][player.x] != 1) {
                                player.y -= 1;
                            }
                        }
                    break;
                    case leftDirection:
                        for (int i = 0; i < 3; i++) {
                            if (currentMap.map[player.y][player.x - 1] != 1) {
                                player.x -= 1;
                            }
                        }
                    break;
                    case rightDirection:
                        for (int i = 0; i < 3; i++) {
                            if (currentMap.map[player.y][player.x + 1] != 1) {
                                player.x += 1;
                            }
                        }
                    break;
                }
                break;
        }

    }

    private void blinkAnimationDrow(int x,int y){ //анимация блинка ассассина
        int bitmapId = R.drawable.blinkanimation;
        Bitmap bitmap;
        Bitmap cBitmap = BitmapFactory.decodeResource(getContext().getResources(), bitmapId);
        bitmap = Bitmap.createScaledBitmap(
                cBitmap, (int)(GameView.unitX), (int)(GameView.unitY), false);
        cBitmap.recycle();
        canvas.drawBitmap(bitmap, x*GameView.unitX, y*GameView.unitY, paint);
    }

    private void fellBlastDrow(int x,int y){ //анимация взрыва скверны
        int bitmapId = R.drawable.felblast;
        Bitmap bitmap;
        Bitmap cBitmap = BitmapFactory.decodeResource(getContext().getResources(), bitmapId);
        bitmap = Bitmap.createScaledBitmap(
                cBitmap, (int)(GameView.unitX), (int)(GameView.unitY), false);
        cBitmap.recycle();
        canvas.drawBitmap(bitmap, x*GameView.unitX, y*GameView.unitY, paint);
    }

    private void frostNovaDrow(int x,int y){ //анимация ледяного взрыва
        int bitmapId;
        if(mapnumber==9){
            bitmapId= R.drawable.inferno;
        }
        else {
            bitmapId = R.drawable.frostnova;
        }
        Bitmap bitmap;
        Bitmap cBitmap = BitmapFactory.decodeResource(getContext().getResources(), bitmapId);
        bitmap = Bitmap.createScaledBitmap(
                cBitmap, (int)(GameView.unitX), (int)(GameView.unitY), false);
        cBitmap.recycle();
        canvas.drawBitmap(bitmap, x*GameView.unitX, y*GameView.unitY, paint);
    }

    private void projectilesUpdate (ArrayList list){
        ArrayList <Projectile> newlist = list;
        for(int i = 0; i<newlist.size();i++){
            if(tick% newlist.get(i).speed==0){
                if(newlist.get(i).update()){
                    newlist.remove(i);
                    i--;
                }
            }
        }
    }

    private void playerTakeDamage(ArrayList projectileList) {
        ArrayList <Projectile> newlist = projectileList;
        for (int i = 0; i < newlist.size(); i++) {
            if (player.x == newlist.get(i).x && player.y == newlist.get(i).y) {
                if(player_cooldown_invulnerability) {
                    player.hitpoints -= newlist.get(i).damage;
                    player_cooldown_invulnerability = false;
                    player_cooldown_invulnerability_tick = tick;
                }
                if(newlist.get(i).frozen && !player_slowed){ //замедление игрока при попадании ледяной стрелы
                    player_slow_tick=tick;
                    player_slowed=true;
                    player.speed+=8;
                }

                newlist.remove(i);
                i--;
            }
        }
    }

    private void enemyTakeDamage(ArrayList unit, int unit_type, ArrayList <Arrow> arrows, ArrayList <FireBall> fireBalls){
        ArrayList <Unit> UnitList = unit;
        for(int i = 0; i<UnitList.size();i++){
            //урон от стрел игрока
            for(int j=0; j<arrows.size();j++){
                if(UnitList.get(i).x==arrows.get(j).x && UnitList.get(i).y==arrows.get(j).y){
                    UnitList.get(i).hitpoints-=arrows.get(j).damage;
                    if(unit_type==unit_id_truedemonking && PilonList.size()!=0){
                        UnitList.get(i).hitpoints+=arrows.get(j).damage;
                    }
                    arrows.remove(j);
                    j--;
                }
            }
            //урон от фаерболов игрока
            for(int k=0;k<fireBalls.size();k++){
                if(UnitList.get(i).x==fireBalls.get(k).x && UnitList.get(i).y==fireBalls.get(k).y){
                    UnitList.get(i).hitpoints-=fireBalls.get(k).damage;
                    if(unit_type==unit_id_truedemonking && PilonList.size()!=0){
                        UnitList.get(i).hitpoints+=fireBalls.get(k).damage;
                    }
                    fireBalls.remove(k);
                    k--;
                }
            }
            if(UnitList.get(i).hitpoints<=0){
                UnitList.remove(i);
                i--;
                if(mapnumber==8){
                    player.hitpoints+=20;
                }
            }
        }
    }

    private void rangedAOEattack(Unit unit, int unit_type, ArrayList projectile_list){ //способность босса лучника
        int old_direction = unit.direction;
        unit.direction=upDirection;
        rangedAttack(unit,unit_type,projectile_list);
        unit.direction=leftDirection;
        rangedAttack(unit,unit_type,projectile_list);
        unit.direction=downDirection;
        rangedAttack(unit,unit_type,projectile_list);
        unit.direction=rightDirection;
        rangedAttack(unit,unit_type,projectile_list);
        unit.direction=old_direction;
    }

    private void rangedTripleAttack(Unit unit, int unit_type, ArrayList projectile_list){ //способность босса лучника
        int startX= unit.x;
        int startY= unit.y;
        rangedAttack(unit,unit_type,projectile_list);
        switch (unit.direction){
            case downDirection:
            case upDirection:
                if(currentMap.map[unit.y][unit.x-1]!=1){
                    unit.x=startX-1;
                    rangedAttack(unit,unit_type,projectile_list);
                }
                if(currentMap.map[unit.y][unit.x+1]!=1){
                    unit.x=startX+1;
                    rangedAttack(unit,unit_type,projectile_list);
                }
                break;
            case leftDirection:
            case rightDirection:
                if(currentMap.map[unit.y+1][unit.x]!=1){
                    unit.y=startY+1;
                    rangedAttack(unit,unit_type,projectile_list);
                }
                if(currentMap.map[unit.y-1][unit.x]!=1){
                    unit.y=startY-1;
                    rangedAttack(unit,unit_type,projectile_list);
                }
                break;
        }
        unit.x=startX;
        unit.y=startY;
    }

    private void rangedAttack(Unit unit, int unit_type, ArrayList projectile_list){
        Projectile projectile = null;
        Unit attacker = unit;
        ArrayList <Projectile> projectiles = projectile_list;
        switch (unit_type){
            case unit_id_archer:
                Arrow arrow = new Arrow(getContext());
                projectile = arrow;
                break;
            case unit_id_mage:
                FireBall fireball = new FireBall(getContext());
                projectile = fireball;
                break;
            case unit_id_freezemage:
                FrostBolt frostBolt = new FrostBolt(getContext());
                projectile = frostBolt;
                break;
            case unit_id_bossarcher:
                Arrow arrow1 = new Arrow(getContext());
                projectile = arrow1;
                break;
            case unit_id_bossmage:
                FireBall fireBall1 = new FireBall(getContext());
                projectile = fireBall1;
                break;
            case unit_id_bossfreezemage:
                FrostBolt frostBolt1 = new FrostBolt(getContext());
                projectile = frostBolt1;
                break;
            case unit_id_bossfreezemageclone:
                FrostBolt frostBolt2 = new FrostBolt(getContext());
                projectile = frostBolt2;
                projectile.damage=0;
                break;
            case unit_id_demonking:
                FireBall fireBall2 = new FireBall(getContext());
                projectile = fireBall2;
                break;
            case unit_id_truedemonking:
                FireBall fireBall3 = new FireBall(getContext());
                projectile = fireBall3;
                break;
            case unit_id_pilon:
                FireBall fireBall4 = new FireBall(getContext());
                projectile = fireBall4;
                break;
        }

        boolean projectileCreated=false;
        switch (attacker.direction){
            case downDirection:
                if(currentMap.map[attacker.y+1][attacker.x]!=1) {
                    projectile.direction = downDirection;
                    projectile.x = attacker.x;
                    projectile.y = attacker.y + 1;
                    projectile.setDownModel(getContext());
                    projectile.init(getContext());
                    projectileCreated=true;
                }
                break;
            case upDirection:
                if(currentMap.map[attacker.y-1][attacker.x]!=1) {
                    projectile.direction = upDirection;
                    projectile.x = attacker.x;
                    projectile.y = attacker.y - 1;
                    projectile.setUpModel(getContext());
                    projectile.init(getContext());
                    projectileCreated=true;
                }
                break;
            case leftDirection:
                if(currentMap.map[attacker.y][attacker.x-1]!=1) {
                    projectile.direction = leftDirection;
                    projectile.x = attacker.x - 1;
                    projectile.y = attacker.y;
                    projectile.setLeftModel(getContext());
                    projectile.init(getContext());
                    projectileCreated=true;
                }
                break;
            case rightDirection:
                if(currentMap.map[attacker.y][attacker.x+1]!=1) {
                    projectile.direction = rightDirection;
                    projectile.x = attacker.x + 1;
                    projectile.y = attacker.y;
                    projectile.setRightModel(getContext());
                    projectile.init(getContext());
                    projectileCreated=true;
                }
                break;
        }
        if(projectileCreated){
            projectiles.add(projectile);
        }
    }

    private void rangedEnemyMoving (Unit unit,int unit_type, ArrayList projectileList){
        Unit enemy = unit;
        if(enemy.attack_tick+ranged_enemy_cooldown_attack<tick){
            enemy.attack_cooldown=false;
        }
        if(findPlayer(unit)==-1) {//если не нашел игрока, перемещение к игроку
            movingToPlayer(enemy);
        }
        else {
            if (!enemy.attack_cooldown && enemy.direction!=findPlayer(unit)) {//поворот в сторону игрока
                enemy.direction = findPlayer(unit);
                switch (enemy.direction){
                    case downDirection:
                        enemy.setDownModel(getContext());
                        break;
                    case upDirection:
                        enemy.setUpModel(getContext());
                        break;
                    case leftDirection:
                        enemy.setLeftModel(getContext());
                        break;
                    case rightDirection:
                        enemy.setRightModel(getContext());
                        break;
                }
            }
            else if(!enemy.attack_cooldown && enemy.direction==findPlayer(unit)){
                rangedAttack(enemy, unit_type, projectileList);
                enemy.attack_tick = tick;
                enemy.attack_cooldown = true;
            }

        }
    }

    private void bossMoving (Unit unit,int unit_type, ArrayList projectileList){
        Unit enemy = unit;
        //проверка кулдаунов на заклинания боссов
        if(unit_type==unit_id_bossarcher && enemy.spell_tick+boss_aoe_attack_cooldown<tick){
            enemy.spell_cooldown=false;
        }

        if(unit_type==unit_id_bossmage && enemy.spell_tick+boss_fellblast_cooldown<tick){
            enemy.spell_cooldown=false;
        }

        //проверка кулдауна на фростновы фризмага
        if(unit_type==unit_id_bossfreezemage && enemy.spell_tick+boss_frostnova_cooldown<tick){
            enemy.spell_cooldown=false;
        }

        //проверка кулдауна на фростновы фризмага
        if(unit_type==unit_id_bossfreezemage && enemy.spell2_tick+boss_illusions_cooldown<tick){
            enemy.spell2_cooldown=false;
        }

        //проверка кулдауна на призыв подкрепления короля демонов
        if(unit_type==unit_id_demonking && enemy.spell_tick+demonking_reinforcement_cooldown<tick){
            enemy.spell_cooldown=false;
        }
        //проверка кулдауна на огненный шторм короля демонов
        if(unit_type==unit_id_demonking && enemy.spell2_tick+demonking_firestorm_cooldown<tick){
            enemy.spell2_cooldown=false;
        }

        //проверка кулдауна на перегрузку пилонов
        if(unit_type==unit_id_truedemonking && enemy.spell_tick+ pilon_overcharge_cooldown <tick){
            enemy.spell_cooldown=false;
        }

        //проверка кулдауна на инферно
        if(unit_type==unit_id_truedemonking && enemy.spell2_tick+inferno_cooldown<tick){
            enemy.spell2_cooldown=false;
        }

        if(enemy.attack_tick+ranged_enemy_cooldown_attack<tick){
            enemy.attack_cooldown=false;
        }
        if(findPlayer(unit)==-1) {//если не нашел игрока, перемещение к игроку
            movingToPlayer(enemy);
        }
        else {
            if (    !enemy.spell_cooldown && enemy.direction != findPlayer(unit)&& unit_type==unit_id_bossarcher ||
                    !enemy.attack_cooldown && enemy.direction != findPlayer(unit)&& unit_type==unit_id_bossmage ||
                    !enemy.attack_cooldown && enemy.direction != findPlayer(unit)&& unit_type==unit_id_bossfreezemage ||
                    !enemy.attack_cooldown && enemy.direction != findPlayer(unit)&& unit_type==unit_id_demonking ||
                    !enemy.attack_cooldown && enemy.direction != findPlayer(unit)&& unit_type==unit_id_truedemonking
            ) {
                enemy.direction = findPlayer(unit);
                switch (enemy.direction) {
                    case downDirection:
                        enemy.setDownModel(getContext());
                        break;
                    case upDirection:
                        enemy.setUpModel(getContext());
                        break;
                    case leftDirection:
                        enemy.setLeftModel(getContext());
                        break;
                    case rightDirection:
                        enemy.setRightModel(getContext());
                        break;
                }
            } else if ( !enemy.spell_cooldown && enemy.direction == findPlayer(unit)&& unit_type==unit_id_bossarcher ||
                        !enemy.attack_cooldown && enemy.direction == findPlayer(unit)&& unit_type==unit_id_bossmage  ||
                        !enemy.attack_cooldown && enemy.direction == findPlayer(unit)&& unit_type==unit_id_bossfreezemage ||
                        !enemy.attack_cooldown && enemy.direction == findPlayer(unit)&& unit_type==unit_id_demonking ||
                        !enemy.attack_cooldown && enemy.direction == findPlayer(unit)&& unit_type==unit_id_truedemonking
            ) {
                switch (unit_type) { //автоатака боссов
                    case unit_id_bossarcher:
                        int a = 1 + (int) (Math.random() * 2); //диапазон [1;2]
                        switch (a) {
                            case 1:
                                rangedAOEattack(enemy, unit_type, projectileList);
                                enemy.spell_tick = tick;
                                enemy.spell_cooldown = true;
                                break;
                            case 2:
                                rangedTripleAttack(enemy, unit_type, projectileList);
                                enemy.spell_tick = tick;
                                enemy.spell_cooldown = true;
                                break;
                        }
                        break;
                    case unit_id_bossmage:
                        rangedAttack(unit,unit_id_bossmage,FireballList);
                        enemy.attack_tick = tick;
                        enemy.attack_cooldown = true;
                        break;
                    case unit_id_bossfreezemage:
                        rangedAttack(unit,unit_id_bossfreezemage,FrostBoltList);
                        enemy.attack_tick = tick;
                        enemy.attack_cooldown = true;
                        break;
                    case unit_id_demonking:
                        rangedAttack(unit,unit_id_demonking,FireballList);
                        enemy.attack_tick = tick;
                        enemy.attack_cooldown = true;
                        break;
                    case unit_id_truedemonking:
                        rangedAttack(unit,unit_id_truedemonking,FireballList);
                        enemy.attack_tick = tick;
                        enemy.attack_cooldown = true;
                        break;

                }


            }
        }
        //пассивная атака босса мага
        if(!enemy.spell_cooldown && unit_type==unit_id_bossmage){
            FellBlast();
            enemy.spell_tick = tick;
            enemy.spell_cooldown = true;
        }
        //пассивная атака босса ледяного мага
        if(!enemy.spell_cooldown && unit_type==unit_id_bossfreezemage){
            FrostNova(unit_id_bossfreezemage);
            enemy.spell_tick = tick;
            enemy.spell_cooldown = true;
        }
        //призыв иллюзий ледяного мага
        if(!enemy.spell2_cooldown && unit_type==unit_id_bossfreezemage){
            SummonIllusions(unit_id_bossfreezemage,BossFreezeMageList);
            enemy.spell2_tick = tick;
            enemy.spell2_cooldown = true;
        }

        //призыв подкреплений короля демонов
        if(!enemy.spell_cooldown && unit_type==unit_id_demonking){
            DemonInvasion();
            enemy.spell_tick = tick;
            enemy.spell_cooldown = true;
        }

        //огненный шторм короля демонов
        if(!enemy.spell2_cooldown && unit_type==unit_id_demonking){
            FireStorm();
            enemy.spell2_tick = tick;
            enemy.spell2_cooldown = true;
        }

        //перегрузка пилонов
        if(!enemy.spell_cooldown && unit_type==unit_id_truedemonking){
            PilonOvercharge();
            enemy.spell_tick = tick;
            enemy.spell_cooldown = true;
        }

        //инферно
        if(!enemy.spell2_cooldown && unit_type==unit_id_truedemonking){
            FrostNova(unit_id_truedemonking);
            enemy.spell2_tick = tick;
            enemy.spell2_cooldown = true;
        }
    }

    private void FellBlast(){
        int i=0;
        fellBlastSpotsList.clear();
        while (fellBlastSpotsList.size()!=25){
            int x = 1 + (int) (Math.random() * 13); //диапазон от 1 до 14
            int y = 1 + (int) (Math.random() * 14); //диапазон от 1 до 15
            if(currentMap.map[y][x]!=1){
                fellBlastSpotsList.add(new ArrayList<Integer>());
                fellBlastSpotsList.get(i).add(x);
                fellBlastSpotsList.get(i).add(y);
                i++;
            }
        }

    }

    private void FrostNova(int unit_type){
        int x;
        int y;
        if(unit_type==unit_id_bossfreezemage) {
            x = player.x;
            y = player.y;
        }
        else {
            x = TrueDemonKingList.get(0).x;
            y = TrueDemonKingList.get(0).y;
        }
        int k=0;
        frostNovaSpots.clear();
        for(int i=x-2;i<x+3;i++){
            for(int j=y-2;j<y+3;j++){
                if((i==x-2 || i==x+2)&&j==y){ //условие для 0 и 4 столбика
                    if(i>-1 && i<15 && j>-1 && j<16) { //проверка на край карты
                        if (currentMap.map[j][i] != 1) {
                            frostNovaSpots.add(new ArrayList<Integer>());
                            frostNovaSpots.get(k).add(i);
                            frostNovaSpots.get(k).add(j);
                            k++;
                        }
                    }
                }

                if(i==x-1 || i==x+1){ //условие для 1 и 3 столбика
                    if(j!=y-2 && j!=y+2) { //
                        if (i > -1 && i < 15 && j > -1 && j < 16) { //проверка на край карты
                            if (currentMap.map[j][i] != 1) {
                                frostNovaSpots.add(new ArrayList<Integer>());
                                frostNovaSpots.get(k).add(i);
                                frostNovaSpots.get(k).add(j);
                                k++;
                            }
                        }
                    }
                }

                if(i==x){ //условие для 3 столбика
                    if(i>-1 && i<15 && j>-1 && j<16) { //проверка на край карты
                        if (currentMap.map[j][i] != 1) {
                            frostNovaSpots.add(new ArrayList<Integer>());
                            frostNovaSpots.get(k).add(i);
                            frostNovaSpots.get(k).add(j);
                            k++;
                        }
                    }
                }
            }
        }
    }

    private void SummonIllusions(int unit_type, ArrayList list){
        switch (unit_type){
            case unit_id_bossfreezemage:
                BossFreezeMageCloneList.clear();
                ArrayList <Unit> units = list;
                int counter=0;
                while (counter<boss_illusions_count){
                    createEnemyUnit(unit_id_bossfreezemageclone,counter,BossFreezeMageCloneList);
                    counter++;
                }
                boolean changed=false;
                while (!changed) { //рандомное перемещение оригинала
                   int x = 1 + (int) (Math.random() * 13); //диапазон от 1 до 14
                   int y = 1 + (int) (Math.random() * 14); //диапазон от 1 до 15
                    if (currentMap.map[y][x] != 1) {
                        units.get(0).x=x;
                        units.get(0).y=y;
                        units.get(0).direction=1;
                        changed=true;
                    }
               }
               break;
                //еще какие-то иллюзии
        }


    }

    private void DemonInvasion(){
        int type = 2 +  (int) (Math.random()*2);
        int spot = (int) (Math.random()*3);
        createEnemyUnit(type,spot,(type==2)?ArcherList:MageList);
    }

    private void FireStorm(){
        int oldX=DemonKingList.get(0).x;
        int oldY=DemonKingList.get(0).y;
        DemonKingList.get(0).x=1;
        DemonKingList.get(0).y=3;
        DemonKingList.get(0).direction=rightDirection;
        rangedAttack(DemonKingList.get(0),unit_id_demonking,FireballList);

        DemonKingList.get(0).x=1;
        DemonKingList.get(0).y=6;
        DemonKingList.get(0).direction=rightDirection;
        rangedAttack(DemonKingList.get(0),unit_id_demonking,FireballList);

        DemonKingList.get(0).x=1;
        DemonKingList.get(0).y=9;
        DemonKingList.get(0).direction=rightDirection;
        rangedAttack(DemonKingList.get(0),unit_id_demonking,FireballList);

        DemonKingList.get(0).x=1;
        DemonKingList.get(0).y=12;
        DemonKingList.get(0).direction=rightDirection;
        rangedAttack(DemonKingList.get(0),unit_id_demonking,FireballList);

        DemonKingList.get(0).x=13;
        DemonKingList.get(0).y=3;
        DemonKingList.get(0).direction=leftDirection;
        rangedAttack(DemonKingList.get(0),unit_id_demonking,FireballList);

        DemonKingList.get(0).x=13;
        DemonKingList.get(0).y=6;
        DemonKingList.get(0).direction=leftDirection;
        rangedAttack(DemonKingList.get(0),unit_id_demonking,FireballList);

        DemonKingList.get(0).x=13;
        DemonKingList.get(0).y=9;
        DemonKingList.get(0).direction=leftDirection;
        rangedAttack(DemonKingList.get(0),unit_id_demonking,FireballList);

        DemonKingList.get(0).x=13;
        DemonKingList.get(0).y=12;
        DemonKingList.get(0).direction=leftDirection;
        rangedAttack(DemonKingList.get(0),unit_id_demonking,FireballList);

        DemonKingList.get(0).x=oldX;
        DemonKingList.get(0).y=oldY;
    }

    private void PilonOvercharge(){
        for(int i=0;i<PilonList.size();i++){
            rangedAOEattack(PilonList.get(i),unit_id_pilon,FireballList);
        }
    }

    private void movingToPlayer(Unit unit){
        Unit enemy = unit;
            if (tick % enemy.speed == 0) {
                Point point = pathfinder(enemy.x,enemy.y); //вызов функции поиска пути, которая возвращает клетку, на которую надо перейти
                if(enemy.x==point.x && enemy.y==point.y+1){
                    if(enemy.direction!=upDirection) {
                        enemy.direction = upDirection;
                        enemy.setUpModel(getContext());
                    }
                    else {
                        enemy.x=point.x;
                        enemy.y=point.y;
                    }
                }
                if(enemy.x==point.x && enemy.y==point.y-1){
                    if(enemy.direction!=downDirection) {
                        enemy.direction = downDirection;
                        enemy.setDownModel(getContext());
                    }
                    else {
                        enemy.x=point.x;
                        enemy.y=point.y;
                    }
                }
                if(enemy.x==point.x+1 && enemy.y==point.y){
                    if(enemy.direction!=leftDirection) {
                        enemy.direction = leftDirection;
                        enemy.setLeftModel(getContext());
                    }
                    else {
                        enemy.x=point.x;
                        enemy.y=point.y;
                    }
                }
                if(enemy.x==point.x-1 && enemy.y==point.y){
                    if(enemy.direction!=rightDirection) {
                        enemy.direction = rightDirection;
                        enemy.setRightModel(getContext());
                    }
                    else {
                        enemy.x=point.x;
                        enemy.y=point.y;
                    }
                }
            }
    }

    private int findPlayer(Unit unit){

        int player_found_direction=-1;
        int startX=unit.x;
        int startY=unit.y;
        int x=startX;
        int y=startY;
        while (currentMap.map[y][x]!=1){
            if(player.x==x && player.y == y){
                player_found_direction=upDirection;
            }
            y--;
        }
        x=startX;
        y=startY;
        while (currentMap.map[y][x]!=1){
            if(player.x==x && player.y == y){
                player_found_direction=downDirection;
            }
            y++;
        }
        x=startX;
        y=startY;
        while (currentMap.map[y][x]!=1){
            if(player.x==x && player.y == y){
                player_found_direction=leftDirection;
            }
            x--;
        }
        x=startX;
        y=startY;
        while (currentMap.map[y][x]!=1){
            if(player.x==x && player.y == y){
                player_found_direction=rightDirection;
            }
            x++;
        }
        return player_found_direction; //возвращает направление в котором находится игрок, если игрока не видно, возвращает -1
    }

    public Point pathfinder(int startX, int startY){

        ArrayList <Point> openSet = new ArrayList<>();
        ArrayList <Point> closedSet = new ArrayList<>();
        Point start = new Point();
        start.x=startX; //Координата Х стартовой точки
        start.y=startY; //Координата У стартовой точки
        start.G=0;      //Цена движения из стартовой точки до текущей
        start.H=Math.abs(player.y-start.y)+Math.abs(player.x-start.x); //Цена движения из текущей точки до конечной
        start.F=start.G+start.H;  //суммарное расстояние
        openSet.add(start); //добавляем начальную точку в открытый список
        for(int i=0;i<16;i++){ //добавляем все непроходимые точки в закрытый список
            for(int j=0;j<15;j++){
                if(currentMap.map[i][j]==1){
                    Point closedPoint = new Point();
                    closedPoint.x=j;
                    closedPoint.y=i;
                    closedSet.add(closedPoint);
                }

            }
        }

        while (!openSet.isEmpty()){
            Point current = new Point(); //создается текущая точка
            current = openSet.get(0);    //ей присваивается первый элемент из открытого списка
            int currentID = 0;           //создается номер точки в открытом списке
            for(int i=1;i<openSet.size();i++){ //текущей становится точка с наименьшим расстоянием до игрока
                if (current.F>openSet.get(i).F){
                    current=openSet.get(i);
                    currentID=i;
                }
            }
            if(current.x==player.x && current.y == player.y){ //если текущая точка равна координате игрока, поиск заканчивается
                Point finalPoint = current; //создается последняя точка
                ArrayList <Point> path_map = new ArrayList<>();
                while (finalPoint!=null){           //пока последнчяя точка существует
                    path_map.add(finalPoint);       //записывается в лист точек
                    finalPoint=finalPoint.parent;   //последней точкой становится ее родитель
                }
                if(path_map.size()>1)               //если размер пути больше 1, возвращается предпоследняя точка
                    return path_map.get(path_map.size()-2);
                else {                              //иначе возвращаются координаты игрока
                    Point returnPoint = new Point();
                    returnPoint.x=player.x;
                    returnPoint.y=player.y;
                    return returnPoint;
                }
            }
            openSet.remove(currentID); //текущая точка удаляется и открытого списка и добавляется в закрытый список
            closedSet.add(current);
                //создание 4 соседних точек и помещение их в массив
            Point rightPoint = new Point();
            rightPoint.x=current.x+1; rightPoint.y=current.y;
            Point leftPoint = new Point();
            leftPoint.x=current.x-1; leftPoint.y=current.y;
            Point upPoint = new Point();
            upPoint.x=current.x; upPoint.y=current.y-1;
            Point downPoint = new Point();
            downPoint.x=current.x; downPoint.y=current.y+1;
            Point[] pointMas = {leftPoint, rightPoint, upPoint, downPoint};
            /*if(mapnumber==9) {
                Random random = new Random();
                for(int i = pointMas.length-1;i>0;i--){
                    int index = random.nextInt(i+1);
                    Point swap = pointMas[index];
                    pointMas[index]=pointMas[i];
                    pointMas[i]=swap;
                }
            }*/


            for(int i =0; i < 4; i++){ //перебор соседних четырех точек
                boolean pointClosed=false; //находится ли точка в закрытом списке
                boolean pointOpen = false; //находится ли точка в открытом списке
                int indexInOpen=0;         //индекс точки в открытом списке
                for(int j=0;j<closedSet.size();j++){ //перебор всех точек в закрытом списке, если точка находится в закрытом списке, берется следующая точка
                    if(closedSet.get(j).x==pointMas[i].x && closedSet.get(j).y==pointMas[i].y ){
                        pointClosed=true;
                    }
                }
                    if(!pointClosed){ //если точка не в закрытом списке
                        pointMas[i].newG = current.G+costMovementPlain; //высчитывается новая стоимость движения к этой точке из начальной
                        for(int k=0; k<openSet.size();k++){ //проверка есть ли точка в открытом списке
                            if(pointMas[i].x==openSet.get(k).x && pointMas[i].y==openSet.get(k).y){
                                pointOpen=true;
                                indexInOpen=k; //если есть, записывается на каком месте
                            }
                        }
                        if(!pointOpen){ //если точка не в открытом списке
                            pointMas[i].G=pointMas[i].newG; //стоимость движения признается лучшей
                            pointMas[i].parent=current;     //устанавливается родитель точки
                            pointMas[i].H=Math.abs(player.y-pointMas[i].y)+Math.abs(player.x-pointMas[i].x);    //устанавливается эврестическая цена перехода от этой точки к финальной
                            pointMas[i].F=pointMas[i].G+pointMas[i].H;  //устанавливается итоговая цена перехода
                            openSet.add(pointMas[i]);                   //и точка записывается в открытый список
                        }
                        else if(pointMas[i].newG < openSet.get(indexInOpen).G ){ //если точка уже в открытом списке, сравнивается старая и новая стоимость движения
                            openSet.get(indexInOpen).parent= current;            //если новая цена меньше старой, перезаписывается родитель
                            openSet.get(indexInOpen).G=pointMas[i].newG;         //цена движения
                            openSet.get(indexInOpen).H=Math.abs(player.y-pointMas[i].y)+Math.abs(player.x-pointMas[i].x); //эврестическая цена движения
                            openSet.get(indexInOpen).F=openSet.get(indexInOpen).G+openSet.get(indexInOpen).H;   //общая цена движения
                        }
                    }
            }
        }
        return null; //ToDo ошибка если не будет прохода к игроку
    }

}
