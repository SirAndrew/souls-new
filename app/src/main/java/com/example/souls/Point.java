package com.example.souls;

public class Point {
    public int x; //координата Х
    public int y; //координата У
    public Point parent; //точка родитель
    public int G; //расстояние из стартовой до текущей
    public int F; //полное расстояние
    public int H; //предположительное расстояние от текущей до финальной
    public int newG; //новое расстояние от стартовой до текущей
    public boolean G_Better=false;
}
