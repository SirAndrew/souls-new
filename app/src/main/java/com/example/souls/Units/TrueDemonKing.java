package com.example.souls.Units;

import android.content.Context;

import com.example.souls.R;

public class TrueDemonKing extends Unit {
    public TrueDemonKing (Context context){
        bitmapId = R.drawable.demonkingbot;
        x=10;
        y=10;
        direction=1;
        speed=18;
        hitpoints=350;
        init(context);
    }

    @Override
    public void setLeftModel(Context context){
        bitmapId= R.drawable.truedemonkingleft;
        init(context);
    }
    @Override
    public void setRightModel(Context context){
        bitmapId= R.drawable.truedemonkingright;
        init(context);
    }
    @Override
    public void setUpModel(Context context){
        bitmapId= R.drawable.truedemonkingtop;
        init(context);
    }
    @Override
    public void setDownModel(Context context){
        bitmapId= R.drawable.truedemonkingbot;
        init(context);
    }
}
