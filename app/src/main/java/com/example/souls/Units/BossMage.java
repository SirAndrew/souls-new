package com.example.souls.Units;

import android.content.Context;

import com.example.souls.R;

public class BossMage extends Unit {

    public BossMage (Context context){
        bitmapId = R.drawable.bossmagedown;
        x=10;
        y=10;
        direction=1;
        speed=16;
        hitpoints=900;
        init(context);
    }

    @Override
    public void setLeftModel(Context context){
        bitmapId= R.drawable.bossmageleft;
        init(context);
    }
    @Override
    public void setRightModel(Context context){
        bitmapId= R.drawable.bossmageright;
        init(context);
    }
    @Override
    public void setUpModel(Context context){
        bitmapId= R.drawable.bossmageup;
        init(context);
    }
    @Override
    public void setDownModel(Context context){
        bitmapId= R.drawable.bossmagedown;
        init(context);
    }

}
