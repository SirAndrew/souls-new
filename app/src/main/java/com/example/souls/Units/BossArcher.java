package com.example.souls.Units;

import android.content.Context;

import com.example.souls.R;

public class BossArcher extends Unit {

    public BossArcher (Context context){
        bitmapId = R.drawable.archer;
        x=10;
        y=10;
        direction=1;
        speed=14;
        hitpoints=600;
        init(context);
    }

    @Override
    public void setLeftModel(Context context){
        bitmapId= R.drawable.bossarcherleft;
        init(context);
    }
    @Override
    public void setRightModel(Context context){
        bitmapId= R.drawable.bossarcherright;
        init(context);
    }
    @Override
    public void setUpModel(Context context){
        bitmapId= R.drawable.bossarcherup;
        init(context);
    }
    @Override
    public void setDownModel(Context context){
        bitmapId= R.drawable.bossarcherdown;
        init(context);
    }

}
