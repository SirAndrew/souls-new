package com.example.souls.Units;

import android.content.Context;

import com.example.souls.R;

import static com.example.souls.GameView.currentMap;
import static com.example.souls.GameView.mapnumber;

public class EnemyArcher extends Unit {

    public EnemyArcher(Context context){
        bitmapId = R.drawable.archer;
        x=10;
        y=10;
        direction=1;
        speed=14;
        hitpoints=100;
        init(context);
    }
    @Override
    public boolean update(){

        if(direction==1 ){
            if(currentMap.map[y][x-1]!=1){
                x--;
            }
            else {
                direction=3;
            }
        }
        if(direction==3 ){
            if(currentMap.map[y][x+1]!=1){
                x++;
            }
            else {
                direction=1;
            }
        }
        return true;
    }

    @Override
    public void setLeftModel(Context context){
        if(mapnumber>5){
            bitmapId= R.drawable.enemyguardianarcherleft;
        }
        else{
            bitmapId= R.drawable.enemyarcherleft;
        }
        init(context);
    }
    @Override
    public void setRightModel(Context context){
        if(mapnumber>5){
            bitmapId= R.drawable.enemyguardianarcherright;
        }
        else{
            bitmapId= R.drawable.enemyarcherright;
        }
        init(context);
    }
    @Override
    public void setUpModel(Context context){
        if(mapnumber>5){
            bitmapId= R.drawable.enemyguardianarchertop;
        }
        else{
            bitmapId= R.drawable.enemyarcherup;
        }
        init(context);
    }
    @Override
    public void setDownModel(Context context){
        if(mapnumber>5){
            bitmapId= R.drawable.enemyguardianarcherbot;
        }
        else{
            bitmapId= R.drawable.enemyarcherdown;
        }
        init(context);
    }
}
