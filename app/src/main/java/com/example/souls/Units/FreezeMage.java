package com.example.souls.Units;

import android.content.Context;

import com.example.souls.R;

import static com.example.souls.GameView.currentMap;

public class FreezeMage extends Unit {
    public FreezeMage(Context context) {
        bitmapId = R.drawable.mage;
        x=10;
        y=10;
        speed=17;
        direction=1;
        hitpoints=100;
        init(context);
    }
    @Override
    public boolean update(){
        if(direction==1 ){
            if(currentMap.map[y][x-1]!=1){
                x--;
            }
            else {
                direction=3;
            }
        }
        if(direction==3 ){
            if(currentMap.map[y][x+1]!=1){
                x++;
            }
            else {
                direction=1;
            }
        }
        return true;
    }
    @Override
    public void setLeftModel(Context context){
        bitmapId= R.drawable.enemyfreezemageleft;
        init(context);
    }
    @Override
    public void setRightModel(Context context){
        bitmapId= R.drawable.enemyfreezemageright;
        init(context);
    }
    @Override
    public void setUpModel(Context context){
        bitmapId= R.drawable.enemyfreezemagetop;
        init(context);
    }
    @Override
    public void setDownModel(Context context){
        bitmapId= R.drawable.enemyfreezemagebot;
        init(context);
    }
}
