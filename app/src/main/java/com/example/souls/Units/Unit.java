package com.example.souls.Units;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.souls.GameView;

public class Unit {
    public int x; // координаты
    public int y;
    public int direction; //временно
    public  int speed;
    public int bitmapId; // id картинки
    public Bitmap bitmap; // картинка
    public int hitpoints;
    public int attack_tick;
    public boolean attack_cooldown=false;
    public boolean spell_cooldown=false;
    public boolean spell2_cooldown=false;
    public int spell_tick;
    public int spell2_tick;

    public void init(Context context) { // сжимаем картинку до нужных размеров
        Bitmap cBitmap = BitmapFactory.decodeResource(context.getResources(), bitmapId);
        bitmap = Bitmap.createScaledBitmap(
                cBitmap, (int)(GameView.unitX), (int)(GameView.unitY), false);
        cBitmap.recycle();
    }

    public boolean update(){ // тут будут вычисляться новые координаты
        return false;
    }

    public void drow(Paint paint, Canvas canvas){ // рисуем картинку
        canvas.drawBitmap(bitmap, x*GameView.unitX, y*GameView.unitY, paint);
    }

    public void setLeftModel(Context context){
    }
    public void setRightModel(Context context){
    }
    public void setUpModel(Context context){

    }
    public void setDownModel(Context context){

    }
}
