package com.example.souls.Units;

import android.content.Context;

import com.example.souls.Game;
import com.example.souls.GameView;
import com.example.souls.R;

import static com.example.souls.GameView.currentMap;

public class Player extends Unit {
    public static int type;
    public static int hitpoints;
    public Player (Context context){
        bitmapId = R.drawable.playermodel;
        x=3;
        y=3;
        speed=7;
        direction=0;
        hitpoints = 100;
        init(context);
    }

    @Override
    public boolean update(){
        if(Game.isLeftPressed&&direction!=1){
            direction=1;
            return true;
        }
        if(Game.isLeftPressed && x > 0 && currentMap.map[y][x-1]!=1){
            x --;
            return true;
        }

        if(Game.isRightPressed&&direction!=3){
            direction=3;
            return true;
        }
        if(Game.isRightPressed && x < GameView.cellX-1&&currentMap.map[y][x+1]!=1){
            x ++;
            return true;
        }
        if(Game.isTopPressed&&direction!=0){
            direction=0;
            return true;
        }
        if(Game.isTopPressed && y > 0 && currentMap.map[y-1][x]!=1){
            y --;
            return true;
        }

        if(Game.isBotPressed&&direction!=2){
            direction=2;
            return true;
        }
        if(Game.isBotPressed && y < GameView.cellY-1 && currentMap.map[y+1][x]!=1){
            y ++;
            return true;
        }
        return false;
    }
}
