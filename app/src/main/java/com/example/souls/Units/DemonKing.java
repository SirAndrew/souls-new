package com.example.souls.Units;

import android.content.Context;

import com.example.souls.R;

public class DemonKing extends Unit {
    public DemonKing (Context context){
        bitmapId = R.drawable.demonkingbot;
        x=10;
        y=10;
        direction=1;
        speed=16;
        hitpoints=1000;
        init(context);
    }

    @Override
    public void setLeftModel(Context context){
        bitmapId= R.drawable.demonkingleft;
        init(context);
    }
    @Override
    public void setRightModel(Context context){
        bitmapId= R.drawable.demonkingright;
        init(context);
    }
    @Override
    public void setUpModel(Context context){
        bitmapId= R.drawable.demonkingtop;
        init(context);
    }
    @Override
    public void setDownModel(Context context){
        bitmapId= R.drawable.demonkingbot;
        init(context);
    }
}
