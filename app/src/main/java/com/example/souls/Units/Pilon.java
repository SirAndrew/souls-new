package com.example.souls.Units;

import android.content.Context;

import com.example.souls.R;

public class Pilon extends Unit {
    public Pilon (Context context){
        bitmapId = R.drawable.pilon;
        x=10;
        y=10;
        direction=1;
        speed=0;
        hitpoints=250;
        init(context);
    }

    @Override
    public void setLeftModel(Context context){
        bitmapId= R.drawable.pilon;
        init(context);
    }
    @Override
    public void setRightModel(Context context){
        bitmapId= R.drawable.pilon;
        init(context);
    }
    @Override
    public void setUpModel(Context context){
        bitmapId= R.drawable.pilon;
        init(context);
    }
    @Override
    public void setDownModel(Context context){
        bitmapId= R.drawable.pilon;
        init(context);
    }
}
