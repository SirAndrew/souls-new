package com.example.souls.Units;

import android.content.Context;

import com.example.souls.R;
import com.example.souls.Units.Unit;

import static com.example.souls.GameView.currentMap;
import static com.example.souls.GameView.mapnumber;

public class EnemyMage extends Unit {

    public EnemyMage(Context context) {
        bitmapId = R.drawable.mage;
        x=10;
        y=10;
        speed=17;
        direction=1;
        hitpoints=100;
        init(context);

    }
    @Override
    public boolean update(){

        if(direction==1 ){
            if(currentMap.map[y][x-1]!=1){
                x--;
            }
            else {
                direction=3;
            }
        }
        if(direction==3 ){
            if(currentMap.map[y][x+1]!=1){
                x++;
            }
            else {
                direction=1;
            }
        }
        return true;
    }
    @Override
    public void setLeftModel(Context context){
        if(mapnumber>5){
            bitmapId= R.drawable.enemyguardianmageleft;
        }
        else{
            bitmapId= R.drawable.enemymageleft;
        }
        init(context);
    }
    @Override
    public void setRightModel(Context context){
        if(mapnumber>5){
            bitmapId= R.drawable.enemyguardianmageright;
        }
        else{
            bitmapId= R.drawable.enemymageright;
        }
        init(context);
    }
    @Override
    public void setUpModel(Context context){
        if(mapnumber>5){
            bitmapId= R.drawable.enemyguardianmagetop;
        }
        else{
            bitmapId= R.drawable.enemymagetop;
        }
        init(context);
    }
    @Override
    public void setDownModel(Context context){
        if(mapnumber>5){
            bitmapId= R.drawable.enemyguardianmagebot;
        }
        else{
            bitmapId= R.drawable.enemymagebot;
        }
        init(context);
    }
}
