package com.example.souls.Units;

import android.content.Context;

import com.example.souls.R;

public class BossFreezeMage extends Unit {
    public BossFreezeMage (Context context){
        bitmapId = R.drawable.mage;
        x=10;
        y=10;
        direction=1;
        speed=13;
        hitpoints=500;
        init(context);
    }

    @Override
    public void setLeftModel(Context context){
        bitmapId= R.drawable.bossfreezemageleft;
        init(context);
    }
    @Override
    public void setRightModel(Context context){
        bitmapId= R.drawable.bossfreezemageright;
        init(context);
    }
    @Override
    public void setUpModel(Context context){
        bitmapId= R.drawable.bossfreezemagetop;
        init(context);
    }
    @Override
    public void setDownModel(Context context){
        bitmapId= R.drawable.bossfreezemagebot;
        init(context);
    }
}
