package com.example.souls.Units;

import android.content.Context;

import com.example.souls.R;

import static com.example.souls.GameView.currentMap;

public class Target extends Unit {
    public Target(Context context){
        bitmapId = R.drawable.target;
        x=13;
        y=14;
        direction=0;
        speed=8;
        hitpoints=1;
        init(context);
    }
    @Override
    public boolean update(){

        if(direction==0 ){
            if(currentMap.map[y-1][x]!=1){
                y--;
            }
            else {
                direction=2;
            }
        }
        if(direction==2 ){
            if(currentMap.map[y+1][x]!=1){
                y++;
            }
            else {
                direction=0;
            }
        }
        return true;
    }
}
