package com.example.souls.Projectiles;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.example.souls.GameView;

public class Projectile {
    public int x; // координаты
    public int y;
    public int direction; //временно
    public  int speed;
    public int damage;
    public boolean frozen=false;
    public int bitmapId; // id картинки
    protected Bitmap bitmap; // картинка


    public void init(Context context) { // сжимаем картинку до нужных размеров
        Bitmap cBitmap = BitmapFactory.decodeResource(context.getResources(), bitmapId);
        bitmap = Bitmap.createScaledBitmap(
                cBitmap, (int)(GameView.unitX), (int)(GameView.unitY), false);
        cBitmap.recycle();
    }

    public boolean update(){
        return false;// тут будут вычисляться новые координаты
    }

    public void drow(Paint paint, Canvas canvas){ // рисуем картинку
        canvas.drawBitmap(bitmap, x*GameView.unitX, y*GameView.unitY, paint);
    }
    public void setLeftModel(Context context){
    }
    public void setRightModel(Context context){

    }
    public void setUpModel(Context context){

    }
    public void setDownModel(Context context){

    }
}
