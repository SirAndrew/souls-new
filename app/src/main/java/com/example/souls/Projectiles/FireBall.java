package com.example.souls.Projectiles;

import android.content.Context;

import com.example.souls.R;

import static com.example.souls.GameView.currentMap;
import static com.example.souls.GameView.mapnumber;

public class FireBall extends Projectile {
    public FireBall (Context context){
        bitmapId = R.drawable.fireballup;
        x=12;
        y=11;
        direction=0; //0 вверх, 1 влево, 2 вниз, 3 вправо
        speed=12;
        damage=50;
        init(context);
    }
    @Override
    public boolean update(){
        switch (direction){
            case 0: if(currentMap.map[y-1][x]!=1){
                y--;
            }
            else{
                return true;
            }
                break;
            case 1: if(currentMap.map[y][x-1]!=1){
                x--;
            }
            else{
                return true;
            }
            break;
            case 2: if(currentMap.map[y+1][x]!=1){
                y++;
            }
            else{
                return true;
            }
            break;
            case 3: if(currentMap.map[y][x+1]!=1){
                x++;
            }
            else{
                return true;
            }
            break;
        }
        return false;
    }
    @Override
    public void setLeftModel(Context context){
        if (mapnumber==4) {
            bitmapId = R.drawable.fellballleft;
        }
        else
            if(mapnumber==9){
                bitmapId=R.drawable.demonfireballleft;
            }
            else
            bitmapId= R.drawable.fireballleft;
        init(context);
    }
    @Override
    public void setRightModel(Context context){
        if (mapnumber==4) {
            bitmapId = R.drawable.fellballright;
        }
        else if(mapnumber==9){
            bitmapId=R.drawable.demonfireballright;
        }
        else
            bitmapId= R.drawable.fireballright;

        init(context);
    }
    @Override
    public void setUpModel(Context context){
        if (mapnumber==4) {
            bitmapId = R.drawable.fellballup;
        }
        else if(mapnumber==9){
            bitmapId=R.drawable.demonfireballup;
        }
        else
            bitmapId= R.drawable.fireballup;

        init(context);
    }
    @Override
    public void setDownModel(Context context){
        if (mapnumber==4) {
            bitmapId = R.drawable.fellballdown;
        }
        else if(mapnumber==9){
            bitmapId=R.drawable.demonfireballdown;
        }
        else
            bitmapId= R.drawable.fireballdown;
        init(context);
    }
}
