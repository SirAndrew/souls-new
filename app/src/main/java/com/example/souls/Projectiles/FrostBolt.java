package com.example.souls.Projectiles;

import android.content.Context;

import com.example.souls.R;

import static com.example.souls.GameView.currentMap;

public class FrostBolt extends Projectile {
    public FrostBolt (Context context){
        bitmapId = R.drawable.frostboltright;
        x=12;
        y=11;
        direction=0; //0 вверх, 1 влево, 2 вниз, 3 вправо
        speed=12;
        damage=30;
        frozen=true;
        init(context);
    }
    @Override
    public boolean update(){
        switch (direction){
            case 0: if(currentMap.map[y-1][x]!=1){
                y--;
            }
            else{
                return true;
            }
                break;
            case 1: if(currentMap.map[y][x-1]!=1){
                x--;
            }
            else{
                return true;
            }
                break;
            case 2: if(currentMap.map[y+1][x]!=1){
                y++;
            }
            else{
                return true;
            }
                break;
            case 3: if(currentMap.map[y][x+1]!=1){
                x++;
            }
            else{
                return true;
            }
                break;
        }
        return false;
    }
    @Override
    public void setLeftModel(Context context){
        bitmapId= R.drawable.frostboltleft;
        init(context);
    }
    @Override
    public void setRightModel(Context context){
        bitmapId= R.drawable.frostboltright;
        init(context);
    }
    @Override
    public void setUpModel(Context context){
        bitmapId= R.drawable.frostboltup;
        init(context);
    }
    @Override
    public void setDownModel(Context context){
        bitmapId= R.drawable.frostboltdown;
        init(context);
    }
}
